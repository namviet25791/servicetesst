﻿using InvoiceApiService.Util;
using System;
using System.Configuration.Install;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;

namespace InvoiceApiService
{
    class Program
    {
        static void Main(string[] args)
        {

            /*ServiceBase[] ServicesToRun;

            if (Environment.UserInteractive)
            {
                if (args.Length > 0)
                {
                    for (int i = 0; i < args.Length; i++)
                    {
                        switch (args[i].ToUpper())
                        {
                            case "/NAME":
                                if (args.Length > i + 1)
                                {
                                    CommonManager.ServiceName = args[++i];
                                }
                                break;
                            case "/I":
                                InstallService();
                                return;
                            case "/U":
                                UninstallService();
                                return;
                            default:
                                break;
                        }
                    }
                }
                else
                {
                    ServiceController service = ServiceController.GetServices().Where(c => c.ServiceName == CommonManager.ServiceName).FirstOrDefault<ServiceController>();

                    if (service == null)
                    {
                        ManagedInstallerClass.InstallHelper(new string[] { Assembly.GetExecutingAssembly().Location });
                    }
                }
            }
            else
            {

                ServicesToRun = new ServiceBase[]
                    {
                        new MinvoiceService()
                    };

                ServiceBase.Run(ServicesToRun);
            }*/
            new MinvoiceService().start();

            /*
            log4net.Config.XmlConfigurator.Configure();

            RabbitClient rabbitClient = RabbitClient.GetInstance();
            rabbitClient.RegisterTasks();

            RedisClient redisClient = RedisClient.GetInstance();
            redisClient.Connect();
            */



        }

        private static bool IsServiceInstalled()
        {
            return ServiceController.GetServices().Any(s => s.ServiceName == CommonManager.ServiceName);
        }

        private static void InstallService()
        {
            if (IsServiceInstalled())
            {
                UninstallService();
            }

            ManagedInstallerClass.InstallHelper(new string[] { Assembly.GetExecutingAssembly().Location });
        }

        private static void UninstallService()
        {
            ManagedInstallerClass.InstallHelper(new string[] { "/u", Assembly.GetExecutingAssembly().Location });
        }
    }
}
