﻿using InvoiceApiService.Data;
using InvoiceApiService.Util;
using Newtonsoft.Json.Linq;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace InvoiceApiService.RbMQ.Tasks
{
    public class vienthongdongbo : IRabbitTask
    {
        private string _queueName = CommonConstants.VIENTHONG_DONGBO_TAODANGKI;

        private IModel _channel;
        //private readonly IMinvoiceDbContext _minvoiceDbContext;

        public vienthongdongbo()
        {
            //this._minvoiceDbContext = new MInvoiceDbContext();
            InitTask();
        }

        private void InitTask()
        {
            RabbitClient client = RabbitClient.GetInstance();
            var conn = client.GetConnection();

            IDictionary<string, object> args = new Dictionary<string, object>();
            args.Add("x-queue-type", "quorum");

            _channel = conn.CreateModel();
            _channel.QueueDeclare(_queueName, true, false, false, args);
            _channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);

            var consumer = new EventingBasicConsumer(_channel);
            consumer.ConsumerTag = Guid.NewGuid().ToString();
            consumer.Received += TaoHoaDon;

            _channel.BasicConsume(_queueName, false, consumer.ConsumerTag, false, false, null, consumer);
        }

        private async void TaoHoaDon(object sender, BasicDeliverEventArgs e)
        {
            var message = Encoding.UTF8.GetString(e.Body);
            var channel = ((EventingBasicConsumer)sender).Model;

            channel.BasicAck(e.DeliveryTag, false);

            IMinvoiceDbContext _minvoiceDbContext = new MInvoiceDbContext();
            await _minvoiceDbContext.BeginTransactionAsync();

            try
            {
                JObject obj = JObject.Parse(message);

                string sql = "";

                if (obj["mau01_68"] != null)
                {
                    JArray lst = (JArray)obj["mau01_68"];
                    foreach (var item in lst)
                    {
                        sql = $@"Delete from #SCHEMA_NAME#.mau01_68 where mau01_id='{item["mau01_id"]}'";
                        await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, new JObject());

                        sql = $@"
                            Insert into #SCHEMA_NAME#.mau01_68 
                            (mau01_id,mso,tnnt,mst,nlhe,dclhe,dctdtu,dtlhe,ddanh,nlap,adhdcma,adhdkma,dkgdqtct,kdndinh,dkgdqtvan,hdgtgt,hdbhang,hdmttien,hdkhac,mdvi,xml_send,xml_rep,dieukhoan,nghidinh,ngay_gui,nguoi_gui,mtdiep_gui,trang_thai,loai,mtdiep_cqt,ket_qua,ngay_tbao,ly_do,cqtqly,htgdlhddtcma,htgdlhddtcmkkhan,htgdlhddtcmkcncao,htgdlhddtkma,htgdlhddtkmttiep,htgdlhddtkmtchuc,ptcdlhdndthdon,ptcdlhdbthop,hdbtscong,hdbhdtqgia,qlnhdon,mdvqhnsach,mcqtqly,ldkunhiem,xml_tiepnhan,mtdiep_tiepnhan_cqt,mtdiep_tiepnhan_gui,lydo_tiepnhan,ngay_tbao_tiepnhan,ketqua_tiepnhan) 
                             VALUES (@mau01_id::uuid,@mso,@tnnt,@mst,@nlhe,@dclhe,@dctdtu,@dtlhe,@ddanh,@nlap::timestamptz,@adhdcma::numeric,@adhdkma::numeric,@dkgdqtct::numeric,@kdndinh,@dkgdqtvan::numeric,@hdgtgt::numeric,@hdbhang::numeric,@hdmttien::numeric,@hdkhac::numeric,@mdvi,@xml_send::xml,@xml_rep::xml,@dieukhoan,@nghidinh,@ngay_gui::timestamptz,@nguoi_gui,@mtdiep_gui,@trang_thai,@loai,@mtdiep_cqt,@ket_qua,@ngay_tbao::timestamp,@ly_do,@cqtqly,@htgdlhddtcma::numeric,@htgdlhddtcmkkhan::numeric,@htgdlhddtcmkcncao::numeric,@htgdlhddtkma::numeric,@htgdlhddtkmttiep::numeric,@htgdlhddtkmtchuc::numeric,@ptcdlhdndthdon::numeric,@ptcdlhdbthop::numeric,@hdbtscong::numeric,@hdbhdtqgia::numeric,@qlnhdon::numeric,@mdvqhnsach,@mcqtqly,@ldkunhiem::int,@xml_tiepnhan::xml,@mtdiep_tiepnhan_cqt,@mtdiep_tiepnhan_gui,@lydo_tiepnhan,@ngay_tbao_tiepnhan::timestamp,@ketqua_tiepnhan)";
                        await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, (JObject)item);

                    }
                }
                if (obj["ketqua_phkt_68"] != null)
                {
                    JArray lst = (JArray)obj["ketqua_phkt_68"];
                    foreach (var item in lst)
                    {
                        sql = $@"Delete from #SCHEMA_NAME#.ketqua_phkt_68 where type_id='{item["type_id"]}'";
                        await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, new JObject());

                        sql = $@"
                            Insert into #SCHEMA_NAME#.ketqua_phkt_68 
                            (ketqua_phkt_68_id,mltdtchieu,type_id,nguoi_gui,tgian_gui,mtdtchieu,note,is_error) 
                            VALUES (@ketqua_phkt_68_id::uuid,@mltdtchieu,@type_id::uuid,@nguoi_gui,@tgian_gui::timestamp,@mtdtchieu,@note,@is_error)";
                        await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, (JObject)item);

                    }
                }
                if (obj["tonghop_gui_tvan_68"] != null)
                {
                    JArray lst = (JArray)obj["tonghop_gui_tvan_68"];
                    foreach (var item in lst)
                    {
                        sql = $@"Delete from #SCHEMA_NAME#.tonghop_gui_tvan_68 where type_id='{item["type_id"]}'";
                        await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, new JObject());

                        sql = $@"
                            Insert into #SCHEMA_NAME#.tonghop_gui_tvan_68 
                            (id,mltdiep_gui,xml_tdiep_gui,type_id,mtdiep_gui,status,message,tgian_gui,nguoi_gui,note,noidung,mdvi,is_api) 
                            VALUES (@id::uuid,@mltdiep_gui,@xml_tdiep_gui::xml,@type_id::uuid,@mtdiep_gui,@status,@message,@tgian_gui::timestamp,@nguoi_gui,@note,@noidung,@mdvi,@is_api)";
                        await _minvoiceDbContext.TransactionCommandAsync(sql, CommandType.Text, (JObject)item);

                    }
                }
                await _minvoiceDbContext.TransactionCommitAsync();
            }
            catch (Exception ex)
            {
                await _minvoiceDbContext.TransactionRollbackAsync();
                Log.Error(_queueName + ": " + ex.Message);
                Log.Error(message);
            }
            finally
            {
                 _minvoiceDbContext.CloseTransaction();

            }
        }

        public void CloseTask()
        {
            _channel.Close();
        }


        public bool IsOpen()
        {
            return _channel.IsOpen;
        }


        public string GetTaskName()
        {
            return _queueName;
        }
    }
}
