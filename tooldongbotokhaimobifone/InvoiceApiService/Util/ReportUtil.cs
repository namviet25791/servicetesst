﻿using DevExpress.XtraReports.UI;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.IO;

namespace InvoiceApiService.Util
{
    public class ReportUtil
    {
        public static byte[] PrintReport(object datasource, string repx, string type)
        {
            byte[] bytes = null;

            XtraReport report = XtraReport.FromFile(repx, true);

            if (datasource != null)
            {
                report.DataSource = datasource;
            }

            if (datasource is DataSet)
            {
                DataSet ds = datasource as DataSet;
                if (ds.Tables.Count > 0)
                {
                    report.DataMember = ds.Tables[0].TableName;
                }
            }

            report.CreateDocument();


            MemoryStream ms = new MemoryStream();

            if (type == "Html")
            {
                report.ExportToHtml(ms);
            }
            else if (type == "Excel" || type == "xlsx")
            {
                report.ExportToXlsx(ms);
            }
            else if (type == "Rtf")
            {
                report.ExportToRtf(ms);
            }
            else
            {
                report.ExportToPdf(ms);
            }

            bytes = ms.ToArray();
            return bytes;

        }

        public static byte[] PrintReport(object datasource, string repx, string type, string tablename)
        {
            byte[] bytes = null;

            XtraReport report = XtraReport.FromFile(repx, true);

            if (datasource != null)
            {
                report.DataSource = datasource;
            }

            if (datasource is DataSet)
            {
                DataSet ds = datasource as DataSet;
                if (ds.Tables.Count > 0)
                {
                    if (string.IsNullOrEmpty(tablename))
                    {
                        report.DataMember = ds.Tables[0].TableName;
                    }
                    else
                    {
                        report.DataMember = ds.Tables[tablename].TableName;
                    }
                }
            }

            report.CreateDocument();


            MemoryStream ms = new MemoryStream();

            if (type == "Html")
            {
                report.ExportToHtml(ms);
            }
            else if (type == "Excel" || type == "xlsx")
            {
                report.ExportToXlsx(ms);
            }
            else if (type == "Rtf")
            {
                report.ExportToRtf(ms);
            }
            else
            {
                report.ExportToPdf(ms);
            }

            bytes = ms.ToArray();
            return bytes;

        }

        public static XtraReport LoadReportFromString(string s)
        {
            XtraReport report = null;

            using (StreamWriter sw = new StreamWriter(new MemoryStream()))
            {
                sw.Write(s.ToString());
                sw.Flush();
                report = XtraReport.FromStream(sw.BaseStream, true);
            }


            return report;
        }

        public static Bitmap DrawDiagonalLine(XtraReport report)
        {
            int PageWidth = report.PageWidth;
            int PageHeight = report.PageHeight;

            Bitmap bmp = new Bitmap(PageWidth, PageHeight);

            using (var graphics = Graphics.FromImage(bmp))
            {
                Pen blackPen = new Pen(Color.Red, 3);
                Point p1 = new Point(0, 0);
                Point p2 = new Point(PageWidth, PageHeight);
                Point p3 = new Point(PageWidth, 0);
                Point p4 = new Point(0, PageHeight);

                if (report.Watermark.Image != null)
                {
                    Image img = report.Watermark.Image;
                    Bitmap b = new Bitmap(img);

                    int transparentcy = report.Watermark.ImageTransparency;

                    if (transparentcy > 0)
                    {

                        b = SetBrightness(b, transparentcy);

                    }


                    Point p5 = new Point((PageWidth - b.Width) / 2, (PageHeight - b.Height) / 2);
                    graphics.DrawImage(b, p5);
                }

                graphics.DrawLine(blackPen, p1, p2);
                graphics.DrawLine(blackPen, p3, p4);
            }

            return bmp;

        }

        public static Bitmap DrawStringDemo(XtraReport report)
        {
            int PageWidth = report.PageWidth;
            int PageHeight = report.PageHeight;

            Bitmap bmp = new Bitmap(PageWidth, PageHeight);

            using (var graphics = Graphics.FromImage(bmp))
            {
                Pen blackPen = new Pen(Color.Red, 3);
                Point p1 = new Point(0, 0);
                Point p2 = new Point(PageWidth, PageHeight);
                Point p3 = new Point(PageWidth, 0);
                Point p4 = new Point(0, PageHeight);

                if (report.Watermark.Image != null)
                {
                    Image img = report.Watermark.Image;
                    Bitmap b = new Bitmap(img);

                    int transparentcy = report.Watermark.ImageTransparency;

                    if (transparentcy > 0)
                    {

                        b = SetBrightness(b, transparentcy);

                    }


                    Point p5 = new Point((PageWidth - b.Width) / 2, (PageHeight - b.Height) / 2);
                    graphics.DrawImage(b, p5);
                }

                RectangleF rectf = new RectangleF(0, 0, bmp.Width, bmp.Height);

                graphics.SmoothingMode = SmoothingMode.AntiAlias;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
                graphics.TextRenderingHint = TextRenderingHint.AntiAliasGridFit;

                string demo = "HÓA ĐƠN DÙNG THỬ";

                Font font = new Font("Arial", 42);

                StringFormat format = new StringFormat()
                {
                    Alignment = StringAlignment.Center,
                    LineAlignment = StringAlignment.Center
                };

                SolidBrush brush = new SolidBrush(Color.FromArgb(255, 230, 230));

                graphics.DrawString(demo, font, brush, rectf, format);
                graphics.Flush();

                //bmp = SetBrightness(bmp, 120);
            }

            return bmp;

        }

        private static Bitmap SetBrightness(Bitmap _currentBitmap, int brightness)
        {
            //Bitmap temp = (Bitmap)_currentBitmap;
            Bitmap bmap = _currentBitmap;
            if (brightness < -255) brightness = -255;
            if (brightness > 255) brightness = 255;
            Color c;
            for (int i = 0; i < bmap.Width; i++)
            {
                for (int j = 0; j < bmap.Height; j++)
                {
                    c = bmap.GetPixel(i, j);
                    int cR = c.R + brightness;
                    int cG = c.G + brightness;
                    int cB = c.B + brightness;

                    if (cR < 0) cR = 1;
                    if (cR > 255) cR = 255;

                    if (cG < 0) cG = 1;
                    if (cG > 255) cG = 255;

                    if (cB < 0) cB = 1;
                    if (cB > 255) cB = 255;

                    bmap.SetPixel(i, j,
                    Color.FromArgb(c.A, (byte)cR, (byte)cG, (byte)cB));
                }
            }


            return bmap;
        }
    }
}