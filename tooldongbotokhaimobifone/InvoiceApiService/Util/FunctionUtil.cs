﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Xml;

namespace InvoiceApiService.Util
{
    public class FunctionUtil
    {
        public static JObject SetKey(JObject parent, JToken token, string newKey, ref JObject output)
        {
            var tokenProp = token as JProperty;
            var oldKeyName = tokenProp.Name;
            parent[newKey] = tokenProp.Value;
            parent.Remove(oldKeyName);
            output = parent;
            return output;
        }

        public static string VND_Amount2(string number)
        {
            if (!string.IsNullOrEmpty(number))
            {
                string[] strTachPhanSauDauPhay;
                if (number.Contains(".") || number.Contains(","))
                {
                    strTachPhanSauDauPhay = number.Split(',', '.');
                    return (So_chu(double.Parse(strTachPhanSauDauPhay[0])).Replace("đồng.", "") + "phẩy " + So_chu(double.Parse(strTachPhanSauDauPhay[1])).ToLower()).Replace("  ", " ");
                }
                return So_chu(double.Parse(number));
            }

            else
            {
                return "";
            }
        }

        public static string VND_Amount(string number)
        {
            string[] strTachPhanSauDauPhay;
            if (number.Contains(".") || number.Contains(","))
            {
                strTachPhanSauDauPhay = number.Split(',', '.');
                return (VND_Amount(strTachPhanSauDauPhay[0]) + "phẩy " + VND_Amount(strTachPhanSauDauPhay[1]));
            }

            string[] dv = { "", "mươi", "trăm", "nghìn", "triệu", "tỉ" };
            string[] cs = { "không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín" };
            string doc;
            int i, j, k, n, len, found, ddv, rd;

            len = number.Length;
            number += "ss";
            doc = "";
            found = 0;
            ddv = 0;
            rd = 0;

            i = 0;
            while (i < len)
            {
                //So chu so o hang dang duyet
                n = (len - i + 2) % 3 + 1;

                //Kiem tra so 0
                found = 0;
                for (j = 0; j < n; j++)
                {
                    if (number[i + j] != '0')
                    {
                        found = 1;
                        break;
                    }
                }

                //Duyet n chu so
                if (found == 1)
                {
                    rd = 1;
                    for (j = 0; j < n; j++)
                    {
                        ddv = 1;
                        switch (number[i + j])
                        {
                            case '0':
                                if (n - j == 3) doc += cs[0] + " ";
                                if (n - j == 2)
                                {
                                    if (number[i + j + 1] != '0') doc += "linh ";
                                    ddv = 0;
                                }
                                break;
                            case '1':
                                if (n - j == 3) doc += cs[1] + " ";
                                if (n - j == 2)
                                {
                                    doc += "mười ";
                                    ddv = 0;
                                }
                                if (n - j == 1)
                                {
                                    if (i + j == 0) k = 0;
                                    else k = i + j - 1;

                                    if (number[k] != '1' && number[k] != '0')
                                        doc += "mốt ";
                                    else
                                        doc += cs[1] + " ";
                                }
                                break;
                            case '5':
                                if ((i + j == len - 1) || (i + j + 3 == len - 1))
                                    doc += "lăm ";
                                else
                                    doc += cs[5] + " ";
                                break;
                            default:
                                doc += cs[(int)number[i + j] - 48] + " ";
                                break;
                        }

                        //Doc don vi nho
                        if (ddv == 1)
                        {
                            doc += ((n - j) != 1) ? dv[n - j - 1] + " " : dv[n - j - 1];
                        }
                    }
                }

                //Doc don vi lon
                if (len - i - n > 0)
                {
                    if ((len - i - n) % 9 == 0)
                    {
                        if (rd == 1)
                            for (k = 0; k < (len - i - n) / 9; k++)
                                doc += "tỉ ";
                        rd = 0;
                    }
                    else
                        if (found != 0) doc += dv[((len - i - n + 1) % 9) / 3 + 2] + " ";
                }

                i += n;
            }

            if (len == 1)
                if (number[0] == '0' || number[0] == '5') return cs[(int)number[0] - 48];

            return doc + " đồng.";
        }

        public static string USD_Amount(string numb)
        {
            string val = "", wholeNo = numb, points = "", andStr = "", pointStr = "";
            string endStr1 = "dollars";
            string endStr = string.Empty;
            try
            {
                int decimalPlace = numb.IndexOf(".");
                if (decimalPlace > 0)
                {
                    wholeNo = numb.Substring(0, decimalPlace);
                    points = numb.Substring(decimalPlace + 1);
                    if (Convert.ToInt32(points) > 0)
                    {
                        andStr = "and";// just to separate whole numbers from points/cents  
                        endStr = "cents";//Cents  
                        pointStr = ConvertWholeNumber(points);
                    }
                }
                val = string.Format("{0} {1} {2} {3} {4}", ConvertWholeNumber(wholeNo).Trim(), endStr1, andStr, pointStr, endStr);
            }
            catch { }
            return val;
        }
        public static String ConvertWholeNumber(String Number)
        {
            string word = "";
            try
            {
                bool beginsZero = false;//tests for 0XX
                bool isDone = false;//test if already translated
                double dblAmt = (Convert.ToDouble(Number));
                //if ((dblAmt > 0) && number.StartsWith("0"))
                if (dblAmt > 0)
                {//test for zero or digit zero in a nuemric
                    beginsZero = Number.StartsWith("0");

                    int numDigits = Number.Length;
                    int pos = 0;//store digit grouping
                    String place = "";//digit grouping name:hundres,thousand,etc...
                    switch (numDigits)
                    {
                        case 1://ones' range

                            word = ones(Number);
                            isDone = true;
                            break;
                        case 2://tens' range
                            word = tens(Number);
                            isDone = true;
                            break;
                        case 3://hundreds' range
                            pos = (numDigits % 3) + 1;
                            place = " Hundred ";
                            break;
                        case 4://thousands' range
                        case 5:
                        case 6:
                            pos = (numDigits % 4) + 1;
                            place = " Thousand ";
                            break;
                        case 7://millions' range
                        case 8:
                        case 9:
                            pos = (numDigits % 7) + 1;
                            place = " Million ";
                            break;
                        case 10://Billions's range
                        case 11:
                        case 12:

                            pos = (numDigits % 10) + 1;
                            place = " Billion ";
                            break;
                        //add extra case options for anything above Billion...
                        default:
                            isDone = true;
                            break;
                    }
                    if (!isDone)
                    {//if transalation is not done, continue...(Recursion comes in now!!)
                        if (Number.Substring(0, pos) != "0" && Number.Substring(pos) != "0")
                        {
                            try
                            {
                                word = ConvertWholeNumber(Number.Substring(0, pos)) + place + ConvertWholeNumber(Number.Substring(pos));
                            }
                            catch { }
                        }
                        else
                        {
                            word = ConvertWholeNumber(Number.Substring(0, pos)) + ConvertWholeNumber(Number.Substring(pos));
                        }

                        //check for trailing zeros
                        //if (beginsZero) word = " and " + word.Trim();
                    }
                    //ignore digit grouping names
                    if (word.Trim().Equals(place.Trim())) word = "";
                }
            }
            catch { }
            return word.Trim();
        }
        public static String tens(String Number)
        {
            int _Number = Convert.ToInt32(Number);
            String name = null;
            switch (_Number)
            {
                case 10:
                    name = "Ten";
                    break;
                case 11:
                    name = "Eleven";
                    break;
                case 12:
                    name = "Twelve";
                    break;
                case 13:
                    name = "Thirteen";
                    break;
                case 14:
                    name = "Fourteen";
                    break;
                case 15:
                    name = "Fifteen";
                    break;
                case 16:
                    name = "Sixteen";
                    break;
                case 17:
                    name = "Seventeen";
                    break;
                case 18:
                    name = "Eighteen";
                    break;
                case 19:
                    name = "Nineteen";
                    break;
                case 20:
                    name = "Twenty";
                    break;
                case 30:
                    name = "Thirty";
                    break;
                case 40:
                    name = "Fourty";
                    break;
                case 50:
                    name = "Fifty";
                    break;
                case 60:
                    name = "Sixty";
                    break;
                case 70:
                    name = "Seventy";
                    break;
                case 80:
                    name = "Eighty";
                    break;
                case 90:
                    name = "Ninety";
                    break;
                default:
                    if (_Number > 0)
                    {
                        name = tens(Number.Substring(0, 1) + "0") + " " + ones(Number.Substring(1));
                    }
                    break;
            }
            return name;
        }
        public static String ones(String Number)
        {
            int _Number = Convert.ToInt32(Number);
            String name = "";
            switch (_Number)
            {
                case 1:
                    name = "One";
                    break;
                case 2:
                    name = "Two";
                    break;
                case 3:
                    name = "Three";
                    break;
                case 4:
                    name = "Four";
                    break;
                case 5:
                    name = "Five";
                    break;
                case 6:
                    name = "Six";
                    break;
                case 7:
                    name = "Seven";
                    break;
                case 8:
                    name = "Eight";
                    break;
                case 9:
                    name = "Nine";
                    break;
            }
            return name;
        }
        public static String ConvertDecimals(String number)
        {
            String cd = "", digit = "", engOne = "";
            for (int i = 0; i < number.Length; i++)
            {
                digit = number[i].ToString();
                if (digit.Equals("0"))
                {
                    engOne = "Zero";
                }
                else
                {
                    engOne = ones(digit);
                }
                cd += " " + engOne;
            }
            return cd;
        }

        public static string ConvertUpper(string s)
        {
            if (String.IsNullOrEmpty(s))
                return s;

            string result = "";

            //lấy danh sách các từ  

            string[] words = s.Split(' ');
            words[0] = words[0].Substring(0, 1).ToUpper() + words[0].Substring(1).ToLower();

            foreach (string word in words)
            {
                // từ nào là các khoảng trắng thừa thì bỏ  
                if (word.Trim() != "")
                {
                    if (word.Length > 1)
                        result += word + " ";
                    else
                        result += word.ToUpper() + " ";
                }

            }
            return result.Trim();
        }
        public static JObject Bathsd(JObject js_details)
        {
            if (js_details["bcthsdhd_id"] != null)
            {
                JToken tk = js_details["bcthsdhd_id"].Parent;
                FunctionUtil.SetKey(js_details, tk, "inv_invoice_report_id", ref js_details);
            }
            if (js_details["loai"] != null)
            {
                JToken tk = js_details["loai"].Parent;
                FunctionUtil.SetKey(js_details, tk, "type", ref js_details);
            }
            if (js_details["ma_dvcs"] != null)
            {
                JToken tk = js_details["ma_dvcs"].Parent;
                FunctionUtil.SetKey(js_details, tk, "branch_code", ref js_details);
            }
            if (js_details["kieu"] != null)
            {
                JToken tk = js_details["kieu"].Parent;
                FunctionUtil.SetKey(js_details, tk, "report_type", ref js_details);
            }
            if (js_details["nam"] != null)
            {
                JToken tk = js_details["nam"].Parent;
                FunctionUtil.SetKey(js_details, tk, "year", ref js_details);
            }
            //if (js_details["thang"] != null)
            //{
            //    JToken tk = model["thang"].Parent;
            //    FunctionUtil.SetKey(model, tk, "type", ref model);
            //}
            if (js_details["tu_ngay"] != null)
            {
                JToken tk = js_details["tu_ngay"].Parent;
                FunctionUtil.SetKey(js_details, tk, "from_date", ref js_details);
            }
            if (js_details["den_ngay"] != null)
            {
                JToken tk = js_details["den_ngay"].Parent;
                FunctionUtil.SetKey(js_details, tk, "to_date", ref js_details);
            }
            if (js_details["phu_luc"] != null)
            {
                JToken tk = js_details["phu_luc"].Parent;
                FunctionUtil.SetKey(js_details, tk, "addendum", ref js_details);
            }
            if (js_details["stt"] != null)
            {
                JToken tk = js_details["stt"].Parent;
                FunctionUtil.SetKey(js_details, tk, "ord", ref js_details);
            }
            if (js_details["mau_hd"] != null)
            {
                JToken tk = js_details["mau_hd"].Parent;
                FunctionUtil.SetKey(js_details, tk, "template_code", ref js_details);
            }
            if (js_details["ky_hieu"] != null)
            {
                JToken tk = js_details["ky_hieu"].Parent;
                FunctionUtil.SetKey(js_details, tk, "invoice_series", ref js_details);
            }
            //if (js_details["ton_dau_ky"] != null)
            //{
            //    JToken tk = model["ton_dau_ky"].Parent;
            //    FunctionUtil.SetKey(model, tk, "opening_quantity", ref model);
            //}
            if (js_details["tu_so1"] != null)
            {
                JToken tk = js_details["tu_so1"].Parent;
                FunctionUtil.SetKey(js_details, tk, "from_number1", ref js_details);
            }
            if (js_details["den_so1"] != null)
            {
                JToken tk = js_details["den_so1"].Parent;
                FunctionUtil.SetKey(js_details, tk, "to_number1", ref js_details);
            }
            if (js_details["tu_so2"] != null)
            {
                JToken tk = js_details["tu_so2"].Parent;
                FunctionUtil.SetKey(js_details, tk, "from_number2", ref js_details);
            }
            if (js_details["den_so2"] != null)
            {
                JToken tk = js_details["den_so2"].Parent;
                FunctionUtil.SetKey(js_details, tk, "to_number2", ref js_details);
            }
            if (js_details["tu_so3"] != null)
            {
                JToken tk = js_details["tu_so3"].Parent;
                FunctionUtil.SetKey(js_details, tk, "from_number3", ref js_details);
            }
            if (js_details["den_so3"] != null)
            {
                JToken tk = js_details["den_so3"].Parent;
                FunctionUtil.SetKey(js_details, tk, "to_number3", ref js_details);
            }
            if (js_details["tu_so4"] != null)
            {
                JToken tk = js_details["tu_so4"].Parent;
                FunctionUtil.SetKey(js_details, tk, "from_number4", ref js_details);
            }
            if (js_details["den_so4"] != null)
            {
                JToken tk = js_details["den_so4"].Parent;
                FunctionUtil.SetKey(js_details, tk, "to_number4", ref js_details);
            }
            if (js_details["cong"] != null)
            {
                JToken tk = js_details["cong"].Parent;
                FunctionUtil.SetKey(js_details, tk, "sum", ref js_details);
            }
            if (js_details["sl_sd"] != null)
            {
                JToken tk = js_details["sl_sd"].Parent;
                FunctionUtil.SetKey(js_details, tk, "using_quantity", ref js_details);
            }
            if (js_details["so_luong1"] != null)
            {
                JToken tk = js_details["so_luong1"].Parent;
                FunctionUtil.SetKey(js_details, tk, "quantity1", ref js_details);
            }
            if (js_details["so1"] != null)
            {
                JToken tk = js_details["so1"].Parent;
                FunctionUtil.SetKey(js_details, tk, "number1", ref js_details);
            }
            if (js_details["so_luong2"] != null)
            {
                JToken tk = js_details["so_luong2"].Parent;
                FunctionUtil.SetKey(js_details, tk, "quantity2", ref js_details);
            }
            if (js_details["so2"] != null)
            {
                JToken tk = js_details["so2"].Parent;
                FunctionUtil.SetKey(js_details, tk, "number2", ref js_details);
            }
            if (js_details["so_luong3"] != null)
            {
                JToken tk = js_details["so_luong3"].Parent;
                FunctionUtil.SetKey(js_details, tk, "quantity3", ref js_details);
            }
            if (js_details["so3"] != null)
            {
                JToken tk = js_details["so3"].Parent;
                FunctionUtil.SetKey(js_details, tk, "number3", ref js_details);
            }
            if (js_details["so_luong4"] != null)
            {
                JToken tk = js_details["so_luong4"].Parent;
                FunctionUtil.SetKey(js_details, tk, "quantity4", ref js_details);
            }
            if (js_details["nguoi_lap"] != null)
            {
                JToken tk = js_details["nguoi_lap"].Parent;
                FunctionUtil.SetKey(js_details, tk, "create_person", ref js_details);
            }
            if (js_details["nguoi_dai_dien"] != null)
            {
                JToken tk = js_details["nguoi_dai_dien"].Parent;
                FunctionUtil.SetKey(js_details, tk, "representative", ref js_details);
            }
            if (js_details["ngay_lap"] != null)
            {
                JToken tk = js_details["ngay_lap"].Parent;
                FunctionUtil.SetKey(js_details, tk, "create_date", ref js_details);
            }
            if (js_details["sl_mua"] != null)
            {
                JToken tk = js_details["sl_mua"].Parent;
                FunctionUtil.SetKey(js_details, tk, "buy_quantity", ref js_details);
            }
            if (js_details["hinhthuc"] != null)
            {
                JToken tk = js_details["hinhthuc"].Parent;
                FunctionUtil.SetKey(js_details, tk, "formality", ref js_details);
            }
            return js_details;
        }

        public static string ComputeSha256Hash(string rawData)
        {
            // Create a SHA256   
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }
        private static string Chu(string gNumber, bool isDonvi = false)
        {
            var result = "";
            switch (gNumber)
            {
                case "0":
                    result = "không";
                    break;

                case "1":
                    result = isDonvi?"mốt" : "một";
                    break;

                case "2":
                    result = "hai";
                    break;

                case "3":
                    result = "ba";
                    break;

                case "4":
                    result = "bốn";
                    break;

                case "5":
                    result = "năm";
                    break;

                case "6":
                    result = "sáu";
                    break;

                case "7":
                    result = "bảy";
                    break;

                case "8":
                    result = "tám";
                    break;

                case "9":
                    result = "chín";
                    break;
            }
            return result;
        }
        private static string Donvi(string so)
        {
            var Kdonvi = "";

            if (so.Equals("1"))
                Kdonvi = "";
            if (so.Equals("2"))
                Kdonvi = "nghìn";
            if (so.Equals("3"))
                Kdonvi = "triệu";
            if (so.Equals("4"))
                Kdonvi = "tỷ";
            if (so.Equals("5"))
                Kdonvi = "nghìn tỷ";
            if (so.Equals("6"))
                Kdonvi = "triệu tỷ";
            if (so.Equals("7"))
                Kdonvi = "tỷ tỷ";

            return Kdonvi;
        }
        private static string Tach(string tach3)
        {
            var Ktach = "";
            if (tach3.Equals("000"))
                return "";
            if (tach3.Length != 3) return Ktach;
            var tr = tach3.Trim().Substring(0, 1).ToString().Trim();
            var ch = tach3.Trim().Substring(1, 1).ToString().Trim();
            var dv = tach3.Trim().Substring(2, 1).ToString().Trim();
            if (tr.Equals("0") && ch.Equals("0"))
                Ktach = " không trăm lẻ " + Chu(dv.ToString().Trim()) + " ";
            if (!tr.Equals("0") && ch.Equals("0") && dv.Equals("0"))
                Ktach = Chu(tr.ToString().Trim()).Trim() + " trăm ";
            if (!tr.Equals("0") && ch.Equals("0") && !dv.Equals("0"))
                Ktach = Chu(tr.ToString().Trim()).Trim() + " trăm linh " + Chu(dv.Trim()).Trim() + " ";
            if (tr.Equals("0") && Convert.ToInt32(ch) > 1 && Convert.ToInt32(dv) > 0 && !dv.Equals("5"))
                Ktach = " không trăm " + Chu(ch.Trim()).Trim() + " mươi " + Chu(dv.Trim()).Trim() + " ";
            if (tr.Equals("0") && Convert.ToInt32(ch) > 1 && dv.Equals("0"))
                Ktach = " không trăm " + Chu(ch.Trim()).Trim() + " mươi ";
            if (tr.Equals("0") && Convert.ToInt32(ch) > 1 && dv.Equals("5"))
                Ktach = " không trăm " + Chu(ch.Trim()).Trim() + " mươi lăm ";
            if (tr.Equals("0") && ch.Equals("1") && Convert.ToInt32(dv) > 0 && !dv.Equals("5"))
                Ktach = " không trăm mười " + Chu(dv.Trim()).Trim() + " ";
            if (tr.Equals("0") && ch.Equals("1") && dv.Equals("0"))
                Ktach = " không trăm mười ";
            if (tr.Equals("0") && ch.Equals("1") && dv.Equals("5"))
                Ktach = " không trăm mười lăm ";
            if (Convert.ToInt32(tr) > 0 && Convert.ToInt32(ch) > 1 && Convert.ToInt32(dv) > 0 && !dv.Equals("5"))
                Ktach = Chu(tr.Trim()).Trim() + " trăm " + Chu(ch.Trim()).Trim() + " mươi " + Chu(dv.Trim()).Trim() + " ";
            if (Convert.ToInt32(tr) > 0 && Convert.ToInt32(ch) > 1 && dv.Equals("0"))
                Ktach = Chu(tr.Trim()).Trim() + " trăm " + Chu(ch.Trim()).Trim() + " mươi ";
            if (Convert.ToInt32(tr) > 0 && Convert.ToInt32(ch) > 1 && dv.Equals("5"))
                Ktach = Chu(tr.Trim()).Trim() + " trăm " + Chu(ch.Trim()).Trim() + " mươi lăm ";
            if (Convert.ToInt32(tr) > 0 && ch.Equals("1") && Convert.ToInt32(dv) > 0 && !dv.Equals("5"))
                Ktach = Chu(tr.Trim()).Trim() + " trăm mười " + Chu(dv.Trim()).Trim() + " ";

            if (Convert.ToInt32(tr) > 0 && ch.Equals("1") && dv.Equals("0"))
                Ktach = Chu(tr.Trim()).Trim() + " trăm mười ";
            if (Convert.ToInt32(tr) > 0 && ch.Equals("1") && dv.Equals("5"))
                Ktach = Chu(tr.Trim()).Trim() + " trăm mười lăm ";

            return Ktach;
        }

        private static string So_chu(double gNum)
        {
            if (gNum == 0)
                return "Không đồng.";

            string lso_chu = "";
            string tach_mod = "";
            string tach_conlai = "";
            double Num = Math.Round(gNum, 0);
            string gN = Convert.ToString(Num, CultureInfo.InvariantCulture);
            int m = Convert.ToInt32(gN.Length / 3);
            int mod = gN.Length - m * 3;
            var dau = "[+]";

            // Dau [+ , - ]
            if (gNum < 0)
                dau = "[-]";
            dau = "";

            switch (mod)
            {
                // Tach hang lon nhat
                case 1:
                    tach_mod = "00" + Convert.ToString(Num.ToString().Trim().Substring(0, 1)).Trim();
                    break;
                case 2:
                    tach_mod = "0" + Convert.ToString(Num.ToString().Trim().Substring(0, 2)).Trim();
                    break;
                case 0:
                    tach_mod = "000";
                    break;
            }

            // Tach hang con lai sau mod :
            if (Num.ToString(CultureInfo.InvariantCulture).Length > 2)
                tach_conlai = Convert.ToString(Num.ToString(CultureInfo.InvariantCulture).Trim().Substring(mod, Num.ToString(CultureInfo.InvariantCulture).Length - mod)).Trim();

            //don vi hang mod
            var im = m + 1;
            if (mod > 0)
                lso_chu = Tach(tach_mod).ToString().Trim() + " " + Donvi(im.ToString().Trim());
            // Tach 3 trong tach_conlai

            var i = m;
            var _m = m;
            var j = 1;

            while (i > 0)
            {
                var tach3 = tach_conlai.Trim().Substring(0, 3).Trim();
                var tach3_ = tach3;
                lso_chu = lso_chu.Trim() + " " + Tach(tach3.Trim()).Trim();
                m = _m + 1 - j;
                if (!tach3_.Equals("000"))
                    lso_chu = lso_chu.Trim() + " " + Donvi(m.ToString().Trim()).Trim();
                tach_conlai = tach_conlai.Trim().Substring(3, tach_conlai.Trim().Length - 3);

                i -= 1;
                j += 1;
            }
            if (lso_chu.Trim().Substring(0, 1).Equals("k"))
                lso_chu = lso_chu.Trim().Substring(10, lso_chu.Trim().Length - 10).Trim();
            if (lso_chu.Trim().Substring(0, 1).Equals("l"))
                lso_chu = lso_chu.Trim().Substring(2, lso_chu.Trim().Length - 2).Trim();
            if (lso_chu.Trim().Length > 0)
                lso_chu = dau.Trim() + " " + lso_chu.Trim().Substring(0, 1).Trim().ToUpper() + lso_chu.Trim().Substring(1, lso_chu.Trim().Length - 1).Trim() + " đồng.";

            return lso_chu.Trim();

        }


        public static String ConvertXml(String xml)
        {

            String temp = xml
            .Replace("minvoice", "HoaDonDienTu")
            .Replace("</invoice>", "</HoaDon>")
            .Replace("<invoice id=\"einvoice\">", "<HoaDon id=\"einvoice\">")
            .Replace("invoiceinformation", "ThongTinHoaDon")
            .Replace("sellerapprecord_id", "SellerAppRecordId")
            .Replace("invoice_name", "TenHoaDon")
            .Replace("template_code", "MauSo")
            .Replace("invoice_series", "Kyhieu")
            .Replace("invoice_number", "SoHoaDon")
            .Replace("invoice_issued_date", "NgayHoaDon")
            .Replace("payment_method_name", "HinhThucThanhToan")
            .Replace("currency_code", "MaTienTe")
            .Replace("exchange_rate", "TyGia")
            .Replace("seller_name", "TenNguoiBan")
            .Replace("seller_taxcode", "MaSoThueNguoiBan")
            .Replace("seller_address", "DiaChiNguoiBan")
            .Replace("seller_tel", "DienThoaiNguoiBan")
            .Replace("seller_bank_account", "TaiKhoanBenBan")
            .Replace("seller_bank_name", "NganHangBenBan")
            .Replace("buyer_display_name", "TenNguoiMua")
            .Replace("buyer_legal_name", "TenDoanhNghiepMua")
            .Replace("buyer_taxcode", "MaSoThueNguoiMua")
            .Replace("buyer_address_line", "DiaChiNguoiMua")
            .Replace("total_amount_without_vat", "TongTienTruocThue")
            .Replace("vat_amount", "TongTienThue")
            .Replace("total_amount", "TongTien")
            .Replace("amount_by_word", "ThanhTienBangChu")
            .Replace("invoicedetail", "ChiTietHoaDon")
            .Replace("<detail>", "<ChiTiet>")
            .Replace("</detail>", "</ChiTiet>")
            .Replace("ord", "stt")
            .Replace("item_code", "MaHang")
            .Replace("item_name", "TenHang")
            .Replace("unit_code", "MaDvt")
            .Replace("quantity", "SoLuong")
            .Replace("unit_price", "DonGia")
            .Replace("vat_percentage", "TienChietKhau")
            .Replace("tax_type", "PhanTramThue")
            .Replace("certified_data", "CertifiedData")
            .Replace("qrcodedata", "qrCodeData")
            .Replace("<signature>", "<ChuKySo>")
            .Replace("</signature>", "</ChuKySo>")
            .Replace("security_number", "SoBaoMat")
            .Replace("signer", "NguoiKy")
            .Replace("signed_date", "NgayKy")
            .Replace("signed_company", "DonViKy")
            .Replace("software_company", "DonViPhanMem");

            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(temp);

            XmlNodeList nodeChiTiets = xmlDocument.SelectNodes("HoaDonDienTu/HoaDon/ChiTietHoaDon/ChiTiet");

            foreach (XmlNode nodeChiTiet in nodeChiTiets)
            {
                nodeChiTiet.InnerXml = nodeChiTiet.InnerXml
                .Replace("TongTienTruocThue", "TienTruocThue")
                .Replace("TongTienThue", "TienThue")
                .Replace("TongTien", "ThanhTien");
            }

            return xmlDocument.InnerXml.ToString(); ;
        }
        public static String ReverseXml(String xml)
        {

            String temp = xml
            .Replace("HoaDonDienTu", "minvoice")
            .Replace("</HoaDon>", "</invoice>")
            .Replace("<HoaDon id=\"einvoice\">", "<invoice id=\"einvoice\">")
            .Replace("ThongTinHoaDon", "invoiceinformation")
            .Replace("SellerAppRecordId", "sellerapprecord_id")
            .Replace("SellerAppRecsttId", "sellerapprecord_id")
            .Replace("TenHoaDon", "invoice_name")
            .Replace("MauSo", "template_code")
            .Replace("Kyhieu", "invoice_series")
            .Replace("SoHoaDon", "invoice_number")
            .Replace("NgayHoaDon", "invoice_issued_date")
            .Replace("HinhThucThanhToan", "payment_method_name")
            .Replace("MaTienTe", "currency_code")
            .Replace("TyGia", "exchange_rate")
            .Replace("TenNguoiBan", "seller_name")
            .Replace("MaSoThueNguoiBan", "seller_taxcode")
            .Replace("DiaChiNguoiBan", "seller_address")
            .Replace("DienThoaiNguoiBan", "seller_tel")
            .Replace("TaiKhoanBenBan", "seller_bank_account")
            .Replace("NganHangBenBan", "seller_bank_name")
            .Replace("TenNguoiMua", "buyer_display_name")
            .Replace("TenDoanhNghiepMua", "buyer_legal_name")
            .Replace("MaSoThueNguoiMua", "buyer_taxcode")
            .Replace("DiaChiNguoiMua", "buyer_address_line")
            .Replace("TongTienTruocThue", "total_amount_without_vat")
            .Replace("TongTienThue", "vat_amount")
            .Replace("TongTien", "total_amount")
            .Replace("ThanhTienBangChu", "amount_by_word")
            .Replace("ChiTietHoaDon", "invoicedetail")
            .Replace("<ChiTiet>", "<detail>")
            .Replace("</ChiTiet>", "</detail>")
            .Replace("stt", "ord")
            .Replace("MaHang", "item_code")
            .Replace("TenHang", "item_name")
            .Replace("MaDvt", "unit_code")
            .Replace("SoLuong", "quantity")
            .Replace("DonGia", "unit_price")
            .Replace("TienChietKhau", "vat_percentage")
            .Replace("PhanTramThue", "tax_type")
            .Replace("CertifiedData", "certified_data")
            .Replace("qrCodeData", "qrcodedata")
            .Replace("<ChuKySo>", "<signature>")
            .Replace("</ChuKySo>", "</signature>")
            .Replace("SoBaoMat", "security_number")
            .Replace("NguoiKy", "signer")
            .Replace("NgayKy", "signed_date")
            .Replace("DonViKy", "signed_company")
            .Replace("DonViPhanMem", "software_company")
            .Replace("ord2", "stt2");

            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(temp);

            XmlNodeList nodeChiTiets = xmlDocument.SelectNodes("minvoice/invoice/invoicedetail/detail");

            foreach (XmlNode nodeChiTiet in nodeChiTiets)
            {
                nodeChiTiet.InnerXml = nodeChiTiet.InnerXml
                .Replace("TienTruocThue", "total_amount_without_vat")
                .Replace("TienThue", "vat_amount")
                .Replace("ThanhTien", "total_amount");
            }

            return xmlDocument.InnerXml.ToString(); ;
        }
    }
}