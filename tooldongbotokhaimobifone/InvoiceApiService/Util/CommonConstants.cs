﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceApiService.Util
{
    public class CommonConstants
    {

        public static readonly int TIEP_NHAN_THONG_BAO_UY_NHIEM = 3;
        public static readonly int CHAP_NHAN_UY_NHIEM = 1;
        public static readonly int KHONG_CHAP_NHAN_UY_NHIEM = 2;

        public static readonly string VENDOR = ConfigurationManager.AppSettings["VENDOR"];
        public static readonly string PRIVATE_KEY = ConfigurationManager.AppSettings["PRIVATE_KEY"];
        public static readonly string TVAN_MST = ConfigurationManager.AppSettings["TVAN_MST"];

        public static readonly string QUEUE_VENDOR_SEND_REQUEST_TO_VAN = "vendor_send_request_van";

        public static readonly string VIENTHONG_DONGBO_TAODANGKI = "vienthong_dongbo_taodangki";

        public static readonly string QUEUE_MOBIFONE_INVOICE_OUT = "mobifone_invoice_out" + "_";

        public static readonly string LOI_TOKEN_XML = "201";
        public static readonly string LOI_INVALID_XML = "202";
        public static readonly string LOI_KHONG_THAY_THONG_DIEP = "203";

    }
}
