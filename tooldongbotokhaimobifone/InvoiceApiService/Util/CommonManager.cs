﻿using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Xml;

namespace InvoiceApiService.Util
{
    public class CommonManager
    {
        public static string ServiceName = "MobiFoneInvoiceDongBoNoiBoVienThong";

        public static string SignXml(string idData, string idSigner, string xml, X509Certificate2 cert, string tagSign)
        {

            XmlDocument document = new XmlDocument();
            document.PreserveWhitespace = false;

            try
            {
                xml = xml.Replace("'", "''");
                xml = xml.Replace("\r", "").Replace("\n", "");
                document.LoadXml(xml);

                AsymmetricAlgorithm asymmetricAlgorithm = cert.PrivateKey;

                SignedXml signedXml = new SignedXml(document);
                signedXml.Signature.Id = idSigner;
                signedXml.SigningKey = asymmetricAlgorithm;

                KeyInfo keyInfo = new KeyInfo();

                RSACryptoServiceProvider rsaprovider = (RSACryptoServiceProvider)cert.PublicKey.Key;
                keyInfo.AddClause(new RSAKeyValue((RSA)rsaprovider));

                KeyInfoX509Data keyInfoData = new KeyInfoX509Data(cert);
                keyInfoData.AddSubjectName(cert.SubjectName.Name);
                keyInfo.AddClause(keyInfoData);

                signedXml.KeyInfo = keyInfo;

                Reference reference = new Reference();
                reference.Uri = idData;

                XmlDsigEnvelopedSignatureTransform env = new XmlDsigEnvelopedSignatureTransform();

                reference.AddTransform(env);

                XmlDsigC14NTransform env1 = new XmlDsigC14NTransform();
                reference.AddTransform(env1);

                signedXml.AddReference(reference);

                //thẻ SigningTime
                //XmlDocument xmlObject = new XmlDocument();
                //xmlObject.PreserveWhitespace = false;
                //XmlElement xmlProperties = xmlObject.CreateElement("SignatureProperties", SignedXml.XmlDsigNamespaceUrl);
                //XmlElement xmlProperty = xmlObject.CreateElement("SignatureProperty", SignedXml.XmlDsigNamespaceUrl);
                //XmlElement dataElement = xmlObject.CreateElement("SigningTime", SignedXml.XmlDsigNamespaceUrl);
                //DateTime dt = DateTime.Now;
                //dataElement.AppendChild(xmlObject.CreateTextNode(dt.ToString("yyyy-MM-ddTHH:mm:ss")));
                //xmlProperty.AppendChild(dataElement);
                //xmlProperties.AppendChild(xmlProperty);
                //xmlObject.AppendChild(xmlProperties);
                //DataObject dataObject = new DataObject();
                //dataObject.Data = xmlObject.ChildNodes;
                //signedXml.AddObject(dataObject);

                signedXml.ComputeSignature();

                XmlElement xmlDigitalSignature = signedXml.GetXml();


                XmlNodeList nodeList = document.GetElementsByTagName(tagSign);

                foreach (XmlNode node in nodeList)
                {

                    node.AppendChild(xmlDigitalSignature);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return document.OuterXml;

        }


        public static XmlDocument SignByMInvoice(XmlDocument document, string uri, string tag)
        {
            try
            {

                string certFile = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\Content\Cert\MobiFone.pfx";

                X509Certificate2 cert = new X509Certificate2(certFile, "Mbf@2021");

                AsymmetricAlgorithm asymmetricAlgorithm = cert.PrivateKey;

                SignedXml signedXml = new SignedXml(document);
                //signedXml.Signature.Id = "minvoice";
                signedXml.SigningKey = asymmetricAlgorithm;

                KeyInfo keyInfo = new KeyInfo();

                RSACryptoServiceProvider rsaprovider = (RSACryptoServiceProvider)cert.PublicKey.Key;
                keyInfo.AddClause(new RSAKeyValue((RSA)rsaprovider));

                KeyInfoX509Data keyInfoData = new KeyInfoX509Data(cert);
                keyInfoData.AddSubjectName(cert.SubjectName.Name);
                keyInfo.AddClause(keyInfoData);

                signedXml.KeyInfo = keyInfo;

                Reference reference = new Reference();
                reference.Uri = uri;

                XmlDsigEnvelopedSignatureTransform env = new XmlDsigEnvelopedSignatureTransform();

                reference.AddTransform(env);

                XmlDsigC14NTransform env1 = new XmlDsigC14NTransform();
                reference.AddTransform(env1);

                signedXml.AddReference(reference);

                //thẻ SigningTime
                //XmlDocument xmlObject = new XmlDocument();
                //xmlObject.PreserveWhitespace = false;
                //XmlElement xmlProperties = xmlObject.CreateElement("SignatureProperties", SignedXml.XmlDsigNamespaceUrl);
                //XmlElement xmlProperty = xmlObject.CreateElement("SignatureProperty", SignedXml.XmlDsigNamespaceUrl);
                //XmlElement dataElement = xmlObject.CreateElement("SigningTime", SignedXml.XmlDsigNamespaceUrl);
                //DateTime dt = DateTime.Now;
                //dataElement.AppendChild(xmlObject.CreateTextNode(dt.ToString("yyyy-MM-ddTHH:mm:ss")));
                //xmlProperty.AppendChild(dataElement);
                //xmlProperties.AppendChild(xmlProperty);
                //xmlObject.AppendChild(xmlProperties);
                //DataObject dataObject = new DataObject();
                //dataObject.Data = xmlObject.ChildNodes;
                //signedXml.AddObject(dataObject);

                signedXml.ComputeSignature();

                XmlElement xmlDigitalSignature = signedXml.GetXml();


                XmlNodeList nodeList = document.GetElementsByTagName(tag);

                foreach (XmlNode node in nodeList)
                {

                    node.AppendChild(xmlDigitalSignature);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return document;

        }


        public static string GetMaThongDiep(string mst)
        {
            // lấy dữ liệu xml thông tin chung
            string mtdiep = "";
            mst = CommonManager.removeSpecialCharacter(mst);
            //string mngui = "K" + mst;
            string mngui = mst;
            // generate mã thông điệp
            string guid_id = CommonManager.removeSpecialCharacter(Guid.NewGuid().ToString().ToUpper());
            mtdiep = string.Format("{0}{1}", mngui, guid_id);

            return mtdiep;
        }

        public static string Hash(string input)
        {
            using (SHA1Managed sha1 = new SHA1Managed())
            {
                var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(input));
                var sb = new StringBuilder(hash.Length * 2);

                foreach (byte b in hash)
                {
                    // can be "x2" if you want lowercase
                    sb.Append(b.ToString("x2"));
                }

                return sb.ToString();
            }
        }

        public static string ConvertSoapXml(string xml)
        {
            string soapXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                        + "<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
                        + "<soap12:Body>" + xml + "</soap12:Body></soap12:Envelope>";

            return soapXml;
        }

        public static string GetSoapResult(string xml, string tag)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            return doc.GetElementsByTagName(tag)[0].InnerText;
        }

        public static string ThongDiep(string mst, string mlThongDiep, string xml, string mathongdiep, int soLuong, string mngui)
        {
            string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string fileName = path + "\\Content\\Xml\\ThongDiep.xml";

            string thongDiepXml = "";

            using (var stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                using (var sr = new StreamReader(stream, Encoding.UTF8))
                {
                    thongDiepXml = sr.ReadToEnd();
                }
            }

            XmlDocument doc = new XmlDocument();
            doc.PreserveWhitespace = false;

            doc.LoadXml(thongDiepXml);

            doc.GetElementsByTagName("MNGui")[0].InnerText = mngui;
            doc.GetElementsByTagName("MNNhan")[0].InnerText = "TCT";
            doc.GetElementsByTagName("MLTDiep")[0].InnerText = mlThongDiep;

            doc.GetElementsByTagName("MTDiep")[0].InnerText = mathongdiep;

            doc.GetElementsByTagName("MST")[0].InnerText = mst;
            doc.GetElementsByTagName("SLuong")[0].InnerText = soLuong.ToString();

            XmlDocumentFragment frag = doc.CreateDocumentFragment();
            frag.InnerXml = xml;

            doc.GetElementsByTagName("DLieu")[0].AppendChild(frag);

            return doc.OuterXml;
        }

        public static string ThongDiepPhanHoi(string mlThongDiep, string mathongdiep, string xmlGoc, int tiepNhan, JArray dsLyDo)
        {
            string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string fileName = path + "\\Content\\Xml\\ThongDiepPhanHoiKyThuat.xml";

            string thongDiepXml = "";

            using (var stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                using (var sr = new StreamReader(stream, Encoding.UTF8))
                {
                    thongDiepXml = sr.ReadToEnd();
                }
            }

            XmlDocument docGoc = new XmlDocument();
            docGoc.PreserveWhitespace = false;
            docGoc.LoadXml(xmlGoc);

            string mnguiGoc = docGoc.GetElementsByTagName("MNGui")[0].InnerText;
            string mnnhanGoc = docGoc.GetElementsByTagName("MNNhan")[0].InnerText;
            string mtdiepGoc = docGoc.GetElementsByTagName("MTDiep")[0].InnerText;

            XmlDocument doc = new XmlDocument();
            doc.PreserveWhitespace = false;

            doc.LoadXml(thongDiepXml);
            doc.GetElementsByTagName("MNGui")[0].InnerText = mnnhanGoc;
            doc.GetElementsByTagName("MNNhan")[0].InnerText = mnguiGoc;
            doc.GetElementsByTagName("MLTDiep")[0].InnerText = mlThongDiep;
            doc.GetElementsByTagName("MTDiep")[0].InnerText = mathongdiep;
            doc.GetElementsByTagName("MTDTChieu")[0].InnerText = mtdiepGoc;

            XmlNode nodeDulieu = doc.GetElementsByTagName("DLieu")[0];
            XmlElement nodeTBao = doc.CreateElement("TBao");

            XmlElement nodeMTDiep = doc.CreateElement("MTDiep");
            nodeMTDiep.InnerText = mtdiepGoc;
            XmlElement nodeMNGui = doc.CreateElement("MNGui");
            nodeMNGui.InnerText = mnguiGoc;
            XmlElement nodeNNhan = doc.CreateElement("NNhan");
            nodeNNhan.InnerText = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
            XmlElement nodeTTTNhan = doc.CreateElement("TTTNhan");
            nodeTTTNhan.InnerText = tiepNhan.ToString();

            nodeTBao.AppendChild(nodeMTDiep);
            nodeTBao.AppendChild(nodeMNGui);
            nodeTBao.AppendChild(nodeNNhan);
            nodeTBao.AppendChild(nodeTTTNhan);

            if (dsLyDo != null && dsLyDo.Count > 0)
            {
                XmlElement nodeDSLdo = doc.CreateElement("DSLdo");
                foreach (JObject entryLyDo in dsLyDo)
                {
                    XmlElement nodeLdo = doc.CreateElement("Ldo");
                    foreach (var entryLDo in entryLyDo)
                    {
                        XmlElement el = doc.CreateElement(entryLDo.Key);
                        el.InnerText = entryLDo.Value.ToString();
                        nodeLdo.AppendChild(el);
                    }
                    nodeDSLdo.AppendChild(nodeLdo);
                }
                nodeTBao.AppendChild(nodeDSLdo);
            }

            nodeDulieu.AppendChild(nodeTBao);

            return doc.OuterXml;
        }

        public static string SignThongDiep(string thongDiepXml)
        {
            XmlDocument document = new XmlDocument();
            document.PreserveWhitespace = false;

            try
            {
                thongDiepXml = thongDiepXml.Replace("'", "''");
                thongDiepXml = thongDiepXml.Replace("\r", "").Replace("\n", "");
                document.LoadXml(thongDiepXml);

                string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                string certFile = path + "\\Content\\Cert\\MobiFone.pfx";

                X509Certificate2 cert = new X509Certificate2(certFile, "Mbf@2021");

                AsymmetricAlgorithm asymmetricAlgorithm = cert.PrivateKey;

                SignedXml signedXml = new SignedXml(document);
                signedXml.SigningKey = asymmetricAlgorithm;

                KeyInfo keyInfo = new KeyInfo();

                RSACryptoServiceProvider rsaprovider = (RSACryptoServiceProvider)cert.PublicKey.Key;
                keyInfo.AddClause(new RSAKeyValue((RSA)rsaprovider));

                KeyInfoX509Data keyInfoData = new KeyInfoX509Data(cert);
                keyInfoData.AddSubjectName(cert.SubjectName.Name);
                keyInfo.AddClause(keyInfoData);

                signedXml.KeyInfo = keyInfo;

                Reference reference = new Reference();
                reference.Uri = "";

                XmlDsigEnvelopedSignatureTransform env = new XmlDsigEnvelopedSignatureTransform();
                reference.AddTransform(env);

                XmlDsigC14NTransform env1 = new XmlDsigC14NTransform();
                reference.AddTransform(env1);

                signedXml.AddReference(reference);

                //thẻ SigningTime
                //XmlDocument xmlObject = new XmlDocument();
                //xmlObject.PreserveWhitespace = false;
                //XmlElement xmlProperties = xmlObject.CreateElement("SignatureProperties", SignedXml.XmlDsigNamespaceUrl);
                //XmlElement xmlProperty = xmlObject.CreateElement("SignatureProperty", SignedXml.XmlDsigNamespaceUrl);
                //XmlElement dataElement = xmlObject.CreateElement("SigningTime", SignedXml.XmlDsigNamespaceUrl);
                //DateTime dt = DateTime.Now;
                //dataElement.AppendChild(xmlObject.CreateTextNode(dt.ToString("yyyy-MM-ddTHH:mm:ss")));
                //xmlProperty.AppendChild(dataElement);
                //xmlProperties.AppendChild(xmlProperty);
                //xmlObject.AppendChild(xmlProperties);
                //DataObject dataObject = new DataObject();
                //dataObject.Data = xmlObject.ChildNodes;
                //signedXml.AddObject(dataObject);

                signedXml.ComputeSignature();

                XmlElement xmlDigitalSignature = signedXml.GetXml();

                document.DocumentElement.AppendChild(document.ImportNode(xmlDigitalSignature, true));

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return document.OuterXml;
        }


        public static XmlDocument SignThongDiep(XmlDocument document)
        {

            try
            {
                string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                string certFile = path + "\\Content\\Cert\\MobiFone.pfx";

                X509Certificate2 cert = new X509Certificate2(certFile, "Mbf@2021");

                AsymmetricAlgorithm asymmetricAlgorithm = cert.PrivateKey;

                SignedXml signedXml = new SignedXml(document);
                signedXml.SigningKey = asymmetricAlgorithm;

                KeyInfo keyInfo = new KeyInfo();

                RSACryptoServiceProvider rsaprovider = (RSACryptoServiceProvider)cert.PublicKey.Key;
                keyInfo.AddClause(new RSAKeyValue((RSA)rsaprovider));

                KeyInfoX509Data keyInfoData = new KeyInfoX509Data(cert);
                keyInfoData.AddSubjectName(cert.SubjectName.Name);
                keyInfo.AddClause(keyInfoData);

                signedXml.KeyInfo = keyInfo;

                Reference reference = new Reference();
                reference.Uri = "";

                XmlDsigEnvelopedSignatureTransform env = new XmlDsigEnvelopedSignatureTransform();
                reference.AddTransform(env);

                XmlDsigC14NTransform env1 = new XmlDsigC14NTransform();
                reference.AddTransform(env1);

                signedXml.AddReference(reference);

                //thẻ SigningTime
                //XmlDocument xmlObject = new XmlDocument();
                //xmlObject.PreserveWhitespace = false;
                //XmlElement xmlProperties = xmlObject.CreateElement("SignatureProperties", SignedXml.XmlDsigNamespaceUrl);
                //XmlElement xmlProperty = xmlObject.CreateElement("SignatureProperty", SignedXml.XmlDsigNamespaceUrl);
                //XmlElement dataElement = xmlObject.CreateElement("SigningTime", SignedXml.XmlDsigNamespaceUrl);
                //DateTime dt = DateTime.Now;
                //dataElement.AppendChild(xmlObject.CreateTextNode(dt.ToString("yyyy-MM-ddTHH:mm:ss")));
                //xmlProperty.AppendChild(dataElement);
                //xmlProperties.AppendChild(xmlProperty);
                //xmlObject.AppendChild(xmlProperties);
                //DataObject dataObject = new DataObject();
                //dataObject.Data = xmlObject.ChildNodes;
                //signedXml.AddObject(dataObject);

                signedXml.ComputeSignature();

                XmlElement xmlDigitalSignature = signedXml.GetXml();

                document.DocumentElement.AppendChild(document.ImportNode(xmlDigitalSignature, true));

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return document;
        }

        public static Boolean VerifyXmlFile(String xml)
        {
            // Create a new XML document.
            XmlDocument xmlDocument = new XmlDocument();

            // Format using white spaces.
            xmlDocument.PreserveWhitespace = false;

            // Load the passed XML file into the document. 
            xmlDocument.LoadXml(xml);

            // Create a new SignedXml object and pass it
            // the XML document class.
            SignedXml signedXml = new SignedXml(xmlDocument);

            // Find the "Signature" node and create a new
            // XmlNodeList object.
            XmlNodeList nodeList = xmlDocument.GetElementsByTagName("Signature");

            // Load the signature node.
            signedXml.LoadXml((XmlElement)nodeList[1]);

            // Check the signature and return the result.
            return signedXml.CheckSignature();
        }

        public static string removeSpecialCharacter(string mst)
        {
            return new String(mst.Where(c => Char.IsLetter(c) || Char.IsDigit(c)).ToArray());
        }

        public static string GenerateXmlPhanHoi(string MLTDiep, string Vendor, string Site, string Timestamp, string Token, string DLTDiep)
        {
            string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string fileName = path + "\\Content\\Xml\\XmlPhanHoi.xml";

            string thongDiepXml = "";

            using (var stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                using (var sr = new StreamReader(stream, Encoding.UTF8))
                {
                    thongDiepXml = sr.ReadToEnd();
                }
            }
            XmlDocument doc = new XmlDocument();
            doc.PreserveWhitespace = false;

            doc.LoadXml(thongDiepXml);
            doc.GetElementsByTagName("MLTDiep")[0].InnerText = MLTDiep;
            doc.GetElementsByTagName("Vendor")[0].InnerText = Vendor;
            doc.GetElementsByTagName("Site")[0].InnerText = Site;
            doc.GetElementsByTagName("Timestamp")[0].InnerText = Timestamp;
            doc.GetElementsByTagName("Token")[0].InnerText = Token;
            doc.GetElementsByTagName("DLTDiep")[0].InnerText = DLTDiep;

            return doc.OuterXml;
        }

        public static bool sendSoapService(string url, string xml)
        {
            bool sendOk = false;
            try
            {
                System.Net.ServicePointManager.ServerCertificateValidationCallback += (send, certificate, chain, sslPolicyErrors) => { return true; };
                WebClient client = new WebClient();

                try
                {
                    client.Encoding = Encoding.UTF8;
                    client.Headers.Add(HttpRequestHeader.ContentType, "application/soap+xml; charset=utf-8");

                    string soapXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                        + "<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
                        + "<soap12:Body>" + xml + "</soap12:Body></soap12:Envelope>";

                    string response = client.UploadString(url, xml);

                    sendOk = true;
                    client.Dispose();
                }
                catch (Exception ex)
                {
                    if (client != null)
                    {
                        client.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("RetryCallBack", ex);
            }

            return sendOk;
        }
    }

}
