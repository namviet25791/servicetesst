﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceApiService.Util
{
    public class Log
    {
        private static readonly ILog _log = LogManager.GetLogger("InvoiceApiService");

        public static void Error(object msg, Exception ex)
        {
            _log.Error(msg, ex);
        }

        public static void Error(object msg)
        {
            _log.Error(msg);
        }

        public static void Info(object msg)
        {
            _log.Info(msg);
        }
    }
}
