﻿using InvoiceApiService.RbMQ;
using InvoiceApiService.Util;
using System;
using System.ServiceProcess;

namespace InvoiceApiService
{
    partial class MinvoiceService : ServiceBase
    {

        public MinvoiceService()
        {
            InitializeComponent();
            Init();
        }

        private void Init()
        {
            this.ServiceName = CommonManager.ServiceName;
            log4net.Config.XmlConfigurator.Configure();

        }
        public void start()
        {
            try
            {
                RabbitClient rabbitClient = RabbitClient.GetInstance();
                rabbitClient.Connect();
                rabbitClient.RegisterTasks();

            }
            catch (Exception ex)
            {
                Log.Error("Start Service ", ex);
            }
        }
        protected override void OnStart(string[] args)
        {
            try
            {
                RabbitClient rabbitClient = RabbitClient.GetInstance();
                rabbitClient.Connect();
                rabbitClient.RegisterTasks();

            }
            catch (Exception ex)
            {
                Log.Error("Start Service ", ex);
            }
        }

        protected override void OnStop()
        {
            try
            {
                RabbitClient rabbitClient = RabbitClient.GetInstance();
                rabbitClient.CloseConnection();

            }
            catch (Exception ex)
            {
                Log.Error("Stop Service ", ex);
            }
        }


    }

}
