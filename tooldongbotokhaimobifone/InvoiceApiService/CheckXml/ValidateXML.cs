﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;

namespace InvoiceApiService.CheckXml
{
    public class ValidationXML
    {
        private static readonly Lazy<ValidationXML> _instance  = new Lazy<ValidationXML>(() => new ValidationXML());
        public static ValidationXML Instance
        {
            get
            {
                if (_instance.IsValueCreated)
                {
                    return _instance.Value;
                }
                return _instance.Value;
            }
        }
        public string GetMaTD(string xml)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.PreserveWhitespace = false;
                xmlDoc.LoadXml(xml);
                XmlNodeList mtDiepList = xmlDoc.GetElementsByTagName("MTDiep");
                if (mtDiepList.Count > 0)
                {
                    return mtDiepList[0].InnerText;
                }
                return "";
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        /*<param></param>
        <summary>
            1. Lấy mã thông điệp, validate theo mã thông điệp
            2. Lấy danh sách file xsd theo từng mẫu
            3. Check file xml theo đường dẫn file xsd trong input
            4. Check chữ kí hợp lệ trong file xml
        </summary>
        <result> 
            "ok:. Hợp lệ
            "error". Không hợp lệ và trả ra phần sai
        </result>
         */
        public ResultValidation Validation(string xml,string mtd)
        {

            ResultValidation result = new ResultValidation()
            {
                status = "ok",
                mess ="Thành công"
            };
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.PreserveWhitespace = false;
                xmlDoc.LoadXml(xml);
                if (mtd == "200" || mtd == "203")
                {
                    result = ValidationHD(xmlDoc, xml);
                }
                else
                {
                    result = ValidationMTD(xmlDoc, xml, int.Parse(mtd));
                }
                XmlNodeList mtDiepList = xmlDoc.GetElementsByTagName("MTDiep");
                if (mtDiepList.Count > 0) result.mtDiep = mtDiepList[0].InnerText;
                else
                {
                    result.mtDiep = "";
                }
                return result;
            }
            catch (Exception ex)
            {
                return new ResultValidation()
                {
                    status = "error",
                    mess = "xml không đúng định dạng, hoặc xử lí không đúng",
                    mtDiep = ""
                };
            }
        }


        #region hatm Validation theo mã thông điệp
        private ResultValidation ValidationMTD(XmlDocument xmlDoc, string xml , int NumberFind)
        {
            ResultValidation result = new ResultValidation();
            string pathFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\Content\FileCheck";
            string pathListXsdJSon = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\Content\FileCheck\getFileXSDWithMTD.json";

            IEnumerable<FileVSD> list = JsonConvert.DeserializeObject<IEnumerable<FileVSD>>(File.ReadAllText(pathListXsdJSon));
            FileVSD item = list.FirstOrDefault(x => x.Number == NumberFind);

            if (item != null)
            {
                result = Check(pathFolder + @"\" + item.File, xml);
            }
            else
            {
                result.status = "error";
                result.mess = "Không có mã thông điệp phù hợp";
            }
            return result;
        }
        private ResultValidation Check(string pathXSD, string xml)
        {
            ResultValidation result = new ResultValidation();
            try
            {
                XmlSchemaSet schema = new XmlSchemaSet();
                XDocument doc = XDocument.Parse(Regex.Replace(xml, "(id=)(\")([a-zA-Z0-9 ]*)(\")", ""));
                //XDocument doc = XDocument.Parse(xml);
                schema.Add("", pathXSD);
                schema.Compile();
                doc.Validate(schema, ValidationEventHandler);

                ResultCheckSign resultSign = CheckSign(xml);

                if (!resultSign.result)
                {
                    result.status = "error";
                    if (!string.IsNullOrEmpty(resultSign.sott)) result.mess = string.Format("Chữ kí thứ {0} không hợp lệ, bạn nên kiểm tra tại http://digisign.vn/kiem-tra-chu-ky-so; {1}", resultSign.sott, resultSign.mess);
                    else result.mess = resultSign.mess;
                }
                else
                {
                    result.status = "ok";
                    result.mess = "xml đúng định dạng";
                }
            }
            catch (Exception ex)
            {
                result.status = "error";
                result.mess = ex.Message;
            }
            return result;
        }
        private ResultCheckSign CheckSign(string xml)
        {
            ResultCheckSign result = new ResultCheckSign()
            {
                result = true
            };
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.PreserveWhitespace = false;
            xmlDoc.LoadXml(xml);
            int i = 1;
            int j = 1;
            SignedXml signedXml = new SignedXml(xmlDoc);
            XmlNodeList nodeList = xmlDoc.GetElementsByTagName("Signature");
            foreach (XmlElement element in nodeList)
            {
                XmlNodeList certificates = element.GetElementsByTagName("X509Certificate");
                if (certificates.Count > 0)
                {
                    X509Certificate2 dcert2 = new X509Certificate2(Convert.FromBase64String(certificates[0].InnerText));
                    try
                    {
                        signedXml.LoadXml(element);
                        if (!signedXml.CheckSignature(dcert2, true))
                        {
                            result.result = false;
                            result.sott += "," + i;
                            break;
                        }
                    }
                    catch (Exception ex)
                    {
                        result.result = false;
                        result.mess += string.Format(";Chữ kí {0} bị lỗi {1}", i, ex.Message);
                        break;
                    }
                }
                else
                {
                    result.result = false;
                    result.mess += string.Format(";Không có chữ kí");
                    break;
                }
            }

            return result;
        }

        #endregion


        #region hatm Validate hóa đơn
        private ResultValidation ValidationHD(XmlDocument xmlDoc, string xml)
        {
            ResultValidation result = new ResultValidation();
            XmlNodeList nodeList = xmlDoc.GetElementsByTagName("KHMSHDon");
            string pathFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\Content\FileCheck";
            string pathListXsdJSon = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\Content\FileCheck\getFileWithHD.json";

            if (nodeList.Count > 0)
            {
                XmlNode node = nodeList[0];
                int NumberFind = 0;


                if (!File.Exists(pathListXsdJSon))
                {
                    result.status = "error";
                    result.mess = "Lỗi list file xsd";
                }

                if (!string.IsNullOrEmpty(node.InnerText) && int.TryParse(node.InnerText, out NumberFind))
                {

                    IEnumerable<FileVSD> list = JsonConvert.DeserializeObject<IEnumerable<FileVSD>>(File.ReadAllText(pathListXsdJSon));
                    FileVSD item = list.FirstOrDefault(x => x.Number == NumberFind);

                    if (item != null)
                    {
                        result = CheckHD(pathFolder + @"\" + item.File, xml);
                    }
                    else
                    {
                        result.status = "error";
                        result.mess = "Kí tự mẫu số hóa đơn chỉ từ 1-> 6";
                    }
                }
                else
                {
                    result.status = "error";
                    result.mess = "Kí tự hóa đơn trống";
                }
            }
            else
            {
                result.status = "error";
                result.mess = "Không có kí hiệu mẫu hóa đơn";
            }
            return result;
        }


        private ResultValidation CheckHD(string pathXSD, string xml)
        {
            ResultValidation result = new ResultValidation();
            try
            {
                XmlSchemaSet schema = new XmlSchemaSet();
                XDocument doc = XDocument.Parse(Regex.Replace(xml, "(id=)(\"|')([a-zA-Z0-9 ]*)(\"|')", ""));
                //XDocument doc = XDocument.Parse(xml);
                schema.Add("", pathXSD);
                schema.Compile();
                doc.Validate(schema, ValidationEventHandler);

                ResultCheckSign resultSign = CheckSignHD(xml);

                if (!resultSign.result)
                {
                    result.status = "error";
                    if (!string.IsNullOrEmpty(resultSign.sott)) result.mess = string.Format("Chữ kí thứ {0} không hợp lệ, bạn nên kiểm tra tại http://digisign.vn/kiem-tra-chu-ky-so; {1}", resultSign.sott, resultSign.mess);
                    else result.mess = resultSign.mess;
                }
                else
                {
                    result.status = "ok";
                    result.mess = "File đúng định dạng";
                }
            }
            catch (Exception ex)
            {
                result.status = "error";
                result.mess = ex.Message;
            }
            return result;
        }
        private ResultCheckSign CheckSignHD(string xml)
        {
            ResultCheckSign result = new ResultCheckSign()
            {
                result = true
            };
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.PreserveWhitespace = false;
            xmlDoc.LoadXml(xml);
            XmlNodeList nodeListHD = xmlDoc.GetElementsByTagName("HDon");
            int i = 1;
            int j = 1;
            if (nodeListHD.Count > 0)
            {
                foreach (XmlElement node in nodeListHD)
                {
                    SignedXml signedXml = new SignedXml(node);
                    XmlNodeList nodeList = xmlDoc.GetElementsByTagName("Signature");
                    foreach (XmlElement element in nodeList)
                    {
                        XmlNodeList certificates = element.GetElementsByTagName("X509Certificate");
                        if (certificates.Count > 0)
                        {
                            X509Certificate2 dcert2 = new X509Certificate2(Convert.FromBase64String(certificates[0].InnerText));
                            try
                            {
                                signedXml.LoadXml(element);
                                if (!signedXml.CheckSignature(dcert2, true))
                                {
                                    result.result = false;
                                    result.sott += "," + i;
                                    break;
                                }
                                i++;
                            }
                            catch (Exception ex)
                            {
                                result.result = false;
                                result.mess += string.Format(";Hóa đơn {0} bị lỗi {1}", j, ex.Message);
                                break;
                            }
                        }
                        else
                        {
                            result.result = false;
                            result.mess += string.Format(";Hóa đơn {0} không có chữ kí", j);
                            break;
                        }
                    }
                    j++;
                }
            }
            else
            {
                result.result = false;
                result.mess += string.Format(";Không có hóa đơn trong khai báo");
            }

            return result;
        }

        #endregion


        #region ToolCheck
        private void ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            XmlSeverityType type = XmlSeverityType.Warning;
            if (Enum.TryParse<XmlSeverityType>("Error", out type))
            {
                if (type == XmlSeverityType.Error) throw new Exception(e.Message);
            }
        }



        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        #endregion

    }

}
