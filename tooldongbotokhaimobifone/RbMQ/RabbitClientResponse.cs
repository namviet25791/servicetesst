﻿using InvoiceApiService.RbMQ.Tasks;
using InvoiceApiService.Util;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceApiService.RbMQ
{
    public class RabbitClientResponse : IDisposable
    {
        private IConnection _connection;
        private List<IRabbitTask> _rabbitTasks;

        public RabbitClientResponse()
        {
            Init();
        }

        public void Init()
        {
            Connect();
        }

        public void Connect()
        {
            try
            {
                
                if (IsOpen() == false)
                {
                    var factory = new ConnectionFactory()
                    {
                        HostName = ConfigurationManager.AppSettings["RABBITMQ_RESPONSE_HOST"],
                        UserName = ConfigurationManager.AppSettings["RABBITMQ_RESPONSE_USER"],
                        Password = ConfigurationManager.AppSettings["RABBITMQ_RESPONSE_PASS"]
                    };
                    factory.AutomaticRecoveryEnabled = true;

                    _connection = factory.CreateConnection();

                }
            }
            catch (Exception ex)
            {
                Log.Error("Error: ", ex);
            }

        }

        public bool IsOpen()
        {
            if (_connection == null) return false;

            return _connection.IsOpen;
        }

        public IConnection GetConnection()
        {
            return _connection;
        }

        public void RegisterTasks()
        {
            if (IsOpen())
            {
                if (_rabbitTasks == null)
                {
                    _rabbitTasks = new List<IRabbitTask>();
                }
            }
        }

        public void PublishMessageQueue(string queueName, string msg)
        {
            Connect();
            using (var channel = _connection.CreateModel())
            {
                channel.QueueDeclare(queue: queueName,
                                     durable: true,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                var body = Encoding.UTF8.GetBytes(msg);

                var properties = channel.CreateBasicProperties();
                properties.Persistent = true;

                channel.BasicPublish(exchange: "",
                                     routingKey: queueName,
                                     basicProperties: properties,
                                     body: body);

            }
        }

        public void PublishQueue(string queueName, string msg)
        {
            Connect();
            using (var channel = _connection.CreateModel())
            {
                channel.ExchangeDeclare(queueName + ".msg.reject", "direct");

                IDictionary<string, object> args = new Dictionary<string, object>();
                args.Add("x-dead-letter-exchange", queueName + ".msg.reject");

                channel.QueueDeclare(queue: queueName,
                                     durable: true,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: args);

                var body = Encoding.UTF8.GetBytes(msg);

                var properties = channel.CreateBasicProperties();
                properties.Persistent = true;

                channel.BasicPublish(exchange: "",
                                     routingKey: queueName,
                                     basicProperties: properties,
                                     body: body);

            }
        }

        public void CloseConnection()
        {
            if (_rabbitTasks != null)
            {
                foreach (var task in _rabbitTasks)
                {
                    task.CloseTask();
                }
            }

            _connection.Close();
        }

        private static Lazy<RabbitClientResponse> _rabbitClient = new Lazy<RabbitClientResponse>();

        public static RabbitClientResponse GetInstance()
        {
            if (_rabbitClient == null)
            {
                _rabbitClient = new Lazy<RabbitClientResponse>();
            }

            return _rabbitClient.Value;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_connection != null)
                {
                    if (_connection.IsOpen)
                    {
                        _connection.Close();
                    }

                    _connection.Dispose();
                }

                if (_rabbitTasks != null)
                {
                    _rabbitTasks.Clear();
                    _rabbitTasks = null;
                }
            }

        }
    }
}
