﻿using InvoiceApiService.Data;
using InvoiceApiService.Util;
using Newtonsoft.Json.Linq;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Xml;

namespace InvoiceApiService.RbMQ.Tasks
{
    public class VendorReceiveVanResponse999 : IRabbitTask
    {
        private string _queueName = CommonConstants.QUEUE_VENDOR_RECEIVE_TCT_RESPONSE_999;

        private IModel _channel;

        public VendorReceiveVanResponse999()
        {
            InitTask();
        }

        private void InitTask()
        {
            RabbitClient client = RabbitClient.GetInstance();
            var conn = client.GetConnection();

            _channel = conn.CreateModel();
            _channel.QueueDeclare(_queueName, true, false, false, null);
            _channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);

            var consumer = new EventingBasicConsumer(_channel);
            consumer.ConsumerTag = Guid.NewGuid().ToString();
            consumer.Received += VanConfirmTechnical;

            _channel.BasicConsume(_queueName, false, consumer.ConsumerTag, false, false, null, consumer);
        }

        private async void VanConfirmTechnical(object sender, BasicDeliverEventArgs e)
        {
            var message = Encoding.UTF8.GetString(e.Body);
            var channel = ((EventingBasicConsumer)sender).Model;

            IMinvoiceDbContext _minvoiceDbContext = null;

            try
            {
                _minvoiceDbContext = new MInvoiceDbContext();
                JObject rabbitObj = JObject.Parse(message);
                string xml = rabbitObj["xml"].ToString();
                string siteHddt = rabbitObj["siteHddt"].ToString();

                _minvoiceDbContext.SetSiteHddt(siteHddt);

                //Message thuế trả về ở đây;
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNode mtdiep_gui = doc.SelectSingleNode("/TDiep/TTChung/MTDTChieu");
                XmlNode mltdiep = doc.SelectSingleNode("/TDiep/TTChung/MLTDiep");

                string xmlVanConfirm = doc.GetElementsByTagName("TDiep")[0].OuterXml;

                XmlDocument docVan = new XmlDocument();
                docVan.PreserveWhitespace = true;
                docVan.LoadXml(xml);

                string status_Tiepnhan = docVan.GetElementsByTagName("TTTNhan")[0].InnerText; // 0 : tiếp nhận; 1: không tiếp nhận
                //string ds_Lydo = docVan.GetElementsByTagName("DSLdo")[0].InnerText;

                XmlNodeList ds_Lydo = docVan.GetElementsByTagName("LDo");

                await _minvoiceDbContext.BeginTransactionAsync();

                // get hdon id từ mã thông điệp thuế trả về
                Dictionary<string, object> parameters2 = new Dictionary<string, object>();
                parameters2.Add("mtdiep_gui", mtdiep_gui?.InnerText);

                string sqlSelectt = $"SELECT * FROM #SCHEMA_NAME#.tonghop_gui_tvan_68 WHERE mtdiep_gui = @mtdiep_gui ";

                DataTable tbl = await _minvoiceDbContext.GetDataTableAsync(sqlSelectt, CommandType.Text, parameters2);
                if (tbl.Rows.Count > 0)
                {
                    for (int i = 0; i < tbl.Rows.Count; i++)
                    {
                        string inv_id = "";
                        string mltdiep_gui = "";

                        inv_id = tbl.Rows[i]["type_id"].ToString();
                        mltdiep_gui = tbl.Rows[i]["mltdiep_gui"].ToString();

                        parameters2.Clear();
                        parameters2.Add("status", status_Tiepnhan);
                        parameters2.Add("message", ds_Lydo.Count);
                        parameters2.Add("inv_id", Guid.Parse(inv_id));

                        string sqlUpdate = $"UPDATE #SCHEMA_NAME#.tonghop_gui_tvan_68 SET status=@status, message=@message WHERE type_id = @inv_id";
                        await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, parameters2);

                        // lưu kết quả phản hồi kĩ thuật

                        string sqlInsert2 = $"INSERT INTO #SCHEMA_NAME#.ketqua_phkt_68 (ketqua_phkt_68_id, mltdtchieu, type_id, nguoi_gui, tgian_gui, mtdtchieu, note, is_error) VALUES "
                               + " (@ketqua_phkt_68_id, @mltdtchieu, @type_id, @nguoi_gui, @tgian_gui, @mtdtchieu, @note, @is_error)";

                        Guid ketqua_phkt_68_id = Guid.NewGuid();

                        string is_error = "0";
                        if (status_Tiepnhan != "0")
                        {
                            is_error = "1";

                            // update vào bảng hoadon68
                            sqlUpdate = $"UPDATE #SCHEMA_NAME#.inv_invoiceauth SET error_status = '1' WHERE inv_invoiceauth_id = '" + inv_id + "'";
                            await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, new Dictionary<string, object>());
                        }

                        Dictionary<string, object> dic6 = new Dictionary<string, object>();
                        dic6.Add("ketqua_phkt_68_id", ketqua_phkt_68_id);
                        dic6.Add("mltdtchieu", mltdiep_gui);
                        dic6.Add("type_id", Guid.Parse(inv_id));
                        dic6.Add("nguoi_gui", mltdiep.InnerText == "999V" ? "MobiFone TVAN" : "Cơ quan Thuế");
                        dic6.Add("tgian_gui", DateTime.Now);
                        dic6.Add("mtdtchieu", mtdiep_gui?.InnerText);
                        dic6.Add("note", "phkt");
                        dic6.Add("is_error", is_error);

                        await _minvoiceDbContext.TransactionCommandAsync(sqlInsert2, CommandType.Text, dic6);

                        // nếu không tiếp nhận, update vào bảng tonghopmaloiphanhoi_68
                        if (status_Tiepnhan != "0")
                        {

                            string maloi = "";
                            string mota = "";

                            foreach (XmlNode node in ds_Lydo)
                            {
                                parameters2.Clear();
                                parameters2.Add("id", Guid.NewGuid());
                                parameters2.Add("type_id", Guid.Parse(inv_id));

                                maloi = node["MLoi"]?.InnerText;
                                mota = node["MTa"]?.InnerText;
                                parameters2.Add("maloi", maloi);
                                parameters2.Add("motaloi", mota);
                                parameters2.Add("tgian", DateTime.Now);
                                parameters2.Add("mtdiep_tchieu", mtdiep_gui?.InnerText);
                                parameters2.Add("reference_id", ketqua_phkt_68_id);

                                string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.tonghopmaloiphanhoi_68 (id, type_id, maloi, motaloi, tgian, mtdiep_tchieu, reference_id) VALUES "
                                                    + " (@id ,@type_id, @maloi, @motaloi, @tgian, @mtdiep_tchieu, @reference_id)";
                                await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, parameters2);
                            }
                        }

                    }
                    //}


                }

                await _minvoiceDbContext.TransactionCommitAsync();
                _minvoiceDbContext.CloseTransaction();

                channel.BasicAck(e.DeliveryTag, false);

            }
            catch (Exception ex)
            {
                Log.Error(_queueName + ": " + ex.Message);
                Log.Error(message);

                if (_minvoiceDbContext != null)
                {
                    try
                    {
                        channel.BasicAck(e.DeliveryTag, false);
                    }
                    catch (Exception) { }
                    try
                    {
                        await _minvoiceDbContext.TransactionRollbackAsync();
                    }
                    catch (Exception) { }
                    try
                    {
                        await _minvoiceDbContext.TransactionCommitAsync();
                    }
                    catch (Exception) { }
                    try
                    {
                        _minvoiceDbContext.CloseTransaction();
                    }
                    catch (Exception) { }
                }
            }

        }

        public void CloseTask()
        {
            _channel.Close();
        }


        public bool IsOpen()
        {
            return _channel.IsOpen;
        }


        public string GetTaskName()
        {
            return _queueName;
        }
    }
}
