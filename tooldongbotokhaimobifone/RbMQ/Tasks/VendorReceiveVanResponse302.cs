﻿using InvoiceApiService.Data;
using InvoiceApiService.Util;
using Newtonsoft.Json.Linq;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Xml;

namespace InvoiceApiService.RbMQ.Tasks
{
    public class VendorReceiveVanResponse302 : IRabbitTask
    {
        private string _queueName = CommonConstants.QUEUE_VENDOR_RECEIVE_TCT_RESPONSE_302;

        private string _queueNameConfirm = CommonConstants.QUEUE_VENDOR_SEND_REQUEST_TO_VAN;

        private IModel _channel;

        public VendorReceiveVanResponse302()
        {
            InitTask();
        }

        private void InitTask()
        {
            RabbitClient client = RabbitClient.GetInstance();
            var conn = client.GetConnection();

            _channel = conn.CreateModel();
            _channel.QueueDeclare(_queueName, true, false, false, null);
            _channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);

            var consumer = new EventingBasicConsumer(_channel);
            consumer.ConsumerTag = Guid.NewGuid().ToString();
            consumer.Received += TctConfirmTechnical;

            _channel.BasicConsume(_queueName, false, consumer.ConsumerTag, false, false, null, consumer);
        }


        private async void TctConfirmTechnical(object sender, BasicDeliverEventArgs e)
        {
            var message = Encoding.UTF8.GetString(e.Body);
            var channel = ((EventingBasicConsumer)sender).Model;

            IMinvoiceDbContext _minvoiceDbContext = null;

            try
            {
                _minvoiceDbContext = new MInvoiceDbContext();
                string mltDiepVendorConfirm = "999";
                string mtdVendorConfirm = null;
                string xmlVendorConfirm = null;

                JObject rabbitObj = JObject.Parse(message);
                string xml = rabbitObj["xml"].ToString();
                string siteHddt = rabbitObj["siteHddt"].ToString();
                string vendor = rabbitObj["vendor"].ToString();
                string privateKey = rabbitObj["privateKey"].ToString();

                _minvoiceDbContext.SetSiteHddt(siteHddt);

                //Message thuế trả về ở đây;
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNode ttchung = doc.SelectSingleNode("/TDiep/TTChung");
                XmlNode mltdiep = doc.SelectSingleNode("/TDiep/TTChung/MLTDiep");
                XmlNode mtdiep_gui = doc.SelectSingleNode("/TDiep/TTChung/MTDTChieu");
                XmlNode mtdiep = doc.SelectSingleNode("/TDiep/TTChung/MTDiep");
                XmlNode mst_nnt = doc.SelectSingleNode("/TDiep/TTChung/MST");

                XmlNodeList dsHoaDon = doc.GetElementsByTagName("HDon");
                XmlNode stbao = doc.SelectSingleNode("/TDiep/DLieu/TBao/STBao/So");
                XmlNode nodeNTbao = doc.SelectSingleNode("/TDiep/DLieu/TBao/STBao/NTBao");

                DateTime ntbao = nodeNTbao == null ? DateTime.Now : Convert.ToDateTime(nodeNTbao.InnerText);

                await _minvoiceDbContext.BeginTransactionAsync();

                foreach (XmlNode hoadon in dsHoaDon)
                {
                    XmlNode KHMSHDon = hoadon.SelectSingleNode("KHMSHDon");
                    XmlNode khhdon = hoadon.SelectSingleNode("KHHDon");
                    XmlNode shdon = hoadon.SelectSingleNode("SHDon");
                    XmlNode ngay = hoadon.SelectSingleNode("NLap");
                    XmlNode ladhddt = hoadon.SelectSingleNode("LADHDDT");
                    XmlNode ldo = hoadon.SelectSingleNode("LDo");

                    DateTime ngaylap = Convert.ToDateTime(hoadon.SelectSingleNode("NLap")?.InnerText);

                    Dictionary<string, object> parameters2 = new Dictionary<string, object>();

                    parameters2.Add("khieu", hoadon.SelectSingleNode("KHMSHDon")?.InnerText + hoadon.SelectSingleNode("KHHDon")?.InnerText);
                    parameters2.Add("shdon", hoadon.SelectSingleNode("SHDon")?.InnerText);
                    parameters2.Add("nlap", ngaylap);

                    string sqlSelect_Hdondctt = $"SELECT hdon_id FROM #SCHEMA_NAME#.inv_invoiceauth  \n"
                                           + " WHERE invoice_series =@khieu and invoice_number=@shdon and date_trunc('day', invoice_issued_date)=date_trunc('day', @nlap)";
                    DataTable hd_dctt_Table = await _minvoiceDbContext.GetDataTableAsync(sqlSelect_Hdondctt, CommandType.Text, parameters2);

                    if (hd_dctt_Table.Rows.Count > 0)
                    {
                        foreach (DataRow dataRow in hd_dctt_Table.Rows)
                        {
                            string inv_id = dataRow["hdon_id"].ToString();
                            string mdvi = dataRow["mdvi"]?.ToString();
                           
                            DataTable dmdvcs = await _minvoiceDbContext.GetDataTableAsync("SELECT * FROM #SCHEMA_NAME#.wb_branch");
                            string sqlUpdate = $"UPDATE #SCHEMA_NAME#.inv_invoiceauth SET invoice_status = 4, stbao=@stbao, ntbao=@ntbao, " +
                                $"   mtdiep_gui=@mtdiep_gui, mtdiep_cqt=@mtdiep_cqt \n"
                                + " WHERE inv_invoiceauth_id = @hdon_id";
                            Dictionary<string, object> param = new Dictionary<string, object>();
                            param.Add("hdon_id", Guid.Parse(inv_id));
                            param.Add("mtdiep_gui", mtdiep_gui.InnerText);
                            param.Add("mtdiep_cqt", mtdiep.InnerText);
                            param.Add("stbao", stbao?.InnerText);
                            param.Add("ntbao", ntbao);
                            await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, param);

                            // lưu log thông điệp vào bảng tracking_log_message_cqt_68
                            string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.tracking_log_message_cqt_68 (log_message_id, hdon_id, mtdiep_cqt, mtdiep_gui, xml_cqt, loai_tbao, ttchung, tgian_gui) VALUES "
                                  + " (@log_message_id,@hdon_id,@mtdiep_cqt, @mtdiep_gui, @xml_cqt::xml, @loai_tbao, @ttchung, @tgian_gui)";

                            Dictionary<string, object> param_Insert = new Dictionary<string, object>();
                            param_Insert.Add("log_message_id", Guid.NewGuid());
                            param_Insert.Add("hdon_id", Guid.Parse(inv_id));
                            param_Insert.Add("mtdiep_cqt", mtdiep?.InnerText);
                            param_Insert.Add("mtdiep_gui", mtdiep_gui?.InnerText);
                            param_Insert.Add("xml_cqt", xml);
                            param_Insert.Add("loai_tbao", mltdiep.InnerText);
                            param_Insert.Add("ttchung", ttchung.InnerText);
                            param_Insert.Add("tgian_gui", DateTime.Now);

                            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, param_Insert);


                            // lưu kết quả rà soát hóa đơn
                            string sqlInsert2 = $"INSERT INTO #SCHEMA_NAME#.ketqua_rasoathd_68 (ketqua_rasoathd_68_id, mltdtchieu, mltdiepphoi, type_id, nguoi_gui, tgian_gui, mtdiep_cqt, mtdtchieu, xml_cqt, note) VALUES "
                                   + " (@ketqua_rasoathd_68_id, @mltdtchieu, @mltdiepphoi, @type_id, @nguoi_gui, @tgian_gui, @mtdiep_cqt, @mtdtchieu, @xml_cqt::xml, @note)";

                            Guid ketqua_rasoathd_68_id = Guid.NewGuid();

                            param_Insert.Clear();
                            param_Insert.Add("ketqua_rasoathd_68_id", ketqua_rasoathd_68_id);
                            param_Insert.Add("mltdtchieu", mltdiep.InnerText);
                            param_Insert.Add("mltdiepphoi", "302");
                            param_Insert.Add("type_id", Guid.Parse(inv_id));
                            param_Insert.Add("nguoi_gui", "Cơ quan Thuế");
                            param_Insert.Add("tgian_gui", DateTime.Now);
                            param_Insert.Add("mtdiep_cqt", mtdiep?.InnerText);
                            param_Insert.Add("mtdtchieu", mtdiep_gui?.InnerText);
                            param_Insert.Add("xml_cqt", xml);
                            param_Insert.Add("note", "rasoat");

                            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert2, CommandType.Text, param_Insert);

                            // lưu lý do cần rà soát

                            string lydo = ldo.InnerText;
                            Dictionary<string, object> parameters3 = new Dictionary<string, object>();
                            parameters3.Add("id", Guid.NewGuid());
                            parameters3.Add("type_id", Guid.Parse(inv_id));
                            parameters3.Add("motaloi", lydo);
                            parameters3.Add("mtdiep_tchieu", mtdiep_gui?.InnerText);
                            parameters3.Add("tgian", DateTime.Now);
                            parameters3.Add("reference_id", ketqua_rasoathd_68_id);

                            string sqlInsert1 = $"INSERT INTO #SCHEMA_NAME#.tonghopmaloiphanhoi_68 (id, type_id, motaloi, tgian, mtdiep_tchieu, reference_id) VALUES "
                                                + " (@id ,@type_id, @motaloi, @tgian, @mtdiep_tchieu, @reference_id)";
                            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert1, CommandType.Text, parameters3);

                            // update mã thông điệp vào bảng quanly_mathongdiep_cqt
                            sqlInsert = $"INSERT INTO #SCHEMA_NAME#.quanly_mathongdiep_cqt (id, mtdiep_cqt, xml_cqt, mltdiep, tgian_gui, mdvi) VALUES "
                                          + " (@id ,@mtdiep_cqt, @xml_cqt::xml, @mltdiep, @tgian_gui, @mdvi)";

                            param_Insert.Clear();
                            param_Insert.Add("id", Guid.NewGuid());
                            param_Insert.Add("mtdiep_cqt", mtdiep?.InnerText);
                            param_Insert.Add("xml_cqt", xml);
                            param_Insert.Add("mltdiep", mltdiep.InnerText);
                            param_Insert.Add("tgian_gui", DateTime.Now);
                            param_Insert.Add("mdvi", mdvi);

                            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, param_Insert);

                            Guid id = Guid.NewGuid();

                            string insertDP = $"INSERT INTO #SCHEMA_NAME#.mau04_68 (mau04_id,loai_tbao, tnnt, mst, xml_send, ntbao, mtdiepcqt, mtdiep_gui) VALUES (@mau04_id,@loai_tbao, @tnnt, @mst, @xml_send, @ntbao, @mtdiepcqt, @mtdiep_gui)";
                            Dictionary<string, object> param2 = new Dictionary<string, object>();
                            param2.Add("mau04_id", id);
                            param2.Add("loai_tbao", "5");
                            param2.Add("tnnt", dmdvcs.Rows[0]["name"]);
                            param2.Add("mst", dmdvcs.Rows[0]["tax_code"]);
                            param2.Add("xml_send", doc.OuterXml);
                            param2.Add("ntbao", DateTime.Now);
                            param2.Add("mtdiepcqt", mtdiep.InnerText);
                            param2.Add("mtdiep_gui", mtdiep_gui.InnerText);

                            await _minvoiceDbContext.TransactionCommandAsync(insertDP, CommandType.Text, param2);

                            string insertCT = $"INSERT INTO #SCHEMA_NAME#.mau04_68_chitiet (mau04_id,mau04_chitiet_id,khieu, shdon, ngay, ladhddt, ldo, hdon_id) VALUES "
                                + " (@mau04_id,@mau04_chitiet_id,@khieu, @shdon, @ngay, @ladhddt, @ldo, @hdon_id)";

                            string khieu = KHMSHDon.InnerText + khhdon.InnerText;
                            Dictionary<string, object> param3 = new Dictionary<string, object>();
                            param3.Add("mau04_id", id);
                            param3.Add("mau04_chitiet_id", Guid.NewGuid());
                            param3.Add("khieu", khieu);
                            param3.Add("shdon", shdon.InnerText);
                            param3.Add("ngay", Convert.ToDateTime(ngay.InnerText));
                            param3.Add("ladhddt", Convert.ToInt16(ladhddt.InnerText));
                            param3.Add("ldo", ldo.InnerText);
                            param3.Add("hdon_id", Guid.Parse(inv_id));
                            await _minvoiceDbContext.TransactionCommandAsync(insertCT, CommandType.Text, param3);

                            try
                            {
                                string sqlSelectt = $"SELECT is_api FROM #SCHEMA_NAME#.tonghop_gui_tvan_68 WHERE type_id = @hdon_id ";
                                param.Clear();
                                param.Add("hdon_id", Guid.Parse(inv_id));
                                DataTable tbl = await _minvoiceDbContext.GetDataTableAsync(sqlSelectt, CommandType.Text, param);
                                if (tbl.Rows.Count > 0)
                                {
                                    string is_api = tbl.Rows[0]["is_api"].ToString();
                                    //nếu hóa đơn được gửi từ đơn vị khác qua API publish queue gửi hóa đơn id
                                    if (is_api == "1")
                                    {
                                        string[] stringList = siteHddt.Split('.');
                                        string str_mst = stringList[0].ToString();

                                        RabbitClientResponse rabbitClientRes = RabbitClientResponse.GetInstance();
                                        rabbitClientRes.PublishMessageQueue(CommonConstants.QUEUE_MOBIFONE_INVOICE_OUT + str_mst, inv_id);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Log.Error(ex.Message);
                            }
                        }
                    }
                }

                string insertPhkt = $"INSERT INTO #SCHEMA_NAME#.tonghop_gui_tvan_68_phkt (id, mltdiep_gui, mtdiep_gui, xml_tdiep_gui, mltdtchieu, mtdtchieu, tonghop_gui_tvan_68_id) " +
               $"  VALUES (@id, @mltdiep_gui, @mtdiep_gui, @xml_tdiep_gui, @mltdtchieu, @mtdtchieu, @tonghop_gui_tvan_68_id)";

                mtdVendorConfirm = CommonManager.GetMaThongDiep(mst_nnt.InnerText);
                xmlVendorConfirm = CommonManager.ThongDiepPhanHoi(mltDiepVendorConfirm, mtdVendorConfirm, xml, 0, null);

                Dictionary<string, object> param6 = new Dictionary<string, object>();
                param6.Add("id", Guid.NewGuid());
                param6.Add("mltdiep_gui", mltDiepVendorConfirm);
                param6.Add("mtdiep_gui", mtdVendorConfirm);
                param6.Add("xml_tdiep_gui", xmlVendorConfirm);
                param6.Add("mltdtchieu", mltdiep.InnerText);
                param6.Add("mtdtchieu", mtdiep.InnerText);
                param6.Add("tonghop_gui_tvan_68_id", null);
                await _minvoiceDbContext.TransactionCommandAsync(insertPhkt, CommandType.Text, param6);

                await _minvoiceDbContext.TransactionCommitAsync();
                _minvoiceDbContext.CloseTransaction();

                channel.BasicAck(e.DeliveryTag, false);

                string timestampConfirm = DateTime.Now.ToString("yyyyMMddHHmmssfff");
                string tokenConfirm = FunctionUtil.ComputeSha256Hash(FunctionUtil.ComputeSha256Hash(xmlVendorConfirm) + ":" + mltDiepVendorConfirm + ":" + vendor + ":" + siteHddt + ":" + timestampConfirm + ":" + privateKey);
                string xmlReturn = CommonManager.GenerateXmlPhanHoi(mltDiepVendorConfirm, vendor, siteHddt, timestampConfirm, tokenConfirm, xmlVendorConfirm);

                RabbitClient rabbitClient = RabbitClient.GetInstance();
                rabbitClient.PublishMessageQueue(_queueNameConfirm, xmlReturn);

            }
            catch (Exception ex)
            {
                Log.Error(_queueName + ": " + ex.Message);
                Log.Error(message);

                if (_minvoiceDbContext != null)
                {
                    try
                    {
                        channel.BasicAck(e.DeliveryTag, false);
                    }
                    catch (Exception) { }
                    try
                    {
                        await _minvoiceDbContext.TransactionRollbackAsync();
                    }
                    catch (Exception) { }
                    try
                    {
                        await _minvoiceDbContext.TransactionCommitAsync();
                    }
                    catch (Exception) { }
                    try
                    {
                        _minvoiceDbContext.CloseTransaction();
                    }
                    catch (Exception) { }
                }
            }

        }

        public void CloseTask()
        {
            _channel.Close();
        }


        public bool IsOpen()
        {
            return _channel.IsOpen;
        }


        public string GetTaskName()
        {
            return _queueName;
        }
    }
}
