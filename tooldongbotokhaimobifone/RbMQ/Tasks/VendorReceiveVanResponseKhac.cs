﻿using InvoiceApiService.Data;
using InvoiceApiService.Util;
using Newtonsoft.Json.Linq;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace InvoiceApiService.RbMQ.Tasks
{
    public class VendorReceiveVanResponseKhac : IRabbitTask
    {
        private string _queueName = CommonConstants.QUEUE_VENDOR_RECEIVE_TCT_RESPONSE_KHAC;

        private string _queueNameConfirm = CommonConstants.QUEUE_VENDOR_SEND_REQUEST_TO_VAN;

        private IModel _channel;

        public VendorReceiveVanResponseKhac()
        {
            InitTask();
        }

        private void InitTask()
        {
            RabbitClient client = RabbitClient.GetInstance();
            var conn = client.GetConnection();

            _channel = conn.CreateModel();
            _channel.QueueDeclare(_queueName, true, false, false, null);
            _channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);

            var consumer = new EventingBasicConsumer(_channel);
            consumer.ConsumerTag = Guid.NewGuid().ToString();
            consumer.Received += TctConfirmTechnical;

            _channel.BasicConsume(_queueName, false, consumer.ConsumerTag, false, false, null, consumer);
        }


        private async void TctConfirmTechnical(object sender, BasicDeliverEventArgs e)
        {
            var message = Encoding.UTF8.GetString(e.Body);
            var channel = ((EventingBasicConsumer)sender).Model;

            IMinvoiceDbContext _minvoiceDbContext = null;

            try
            {
                _minvoiceDbContext = new MInvoiceDbContext();
                string mltDiepVendorConfirm = "999";
                string mtdVendorConfirm = null;
                string xmlVendorConfirm = null;

                JObject rabbitObj = JObject.Parse(message);
                string xml = rabbitObj["xml"].ToString();
                string siteHddt = rabbitObj["siteHddt"].ToString();
                string vendor = rabbitObj["vendor"].ToString();
                string privateKey = rabbitObj["privateKey"].ToString();

                _minvoiceDbContext.SetSiteHddt(siteHddt);

                //Message thuế trả về ở đây;
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNode ttchung = doc.SelectSingleNode("/TDiep/TTChung");
                XmlNode mltdiep = doc.SelectSingleNode("/TDiep/TTChung/MLTDiep");
                XmlNode mtdiep_gui = doc.SelectSingleNode("/TDiep/TTChung/MTDTChieu");
                XmlNode mtdiep = doc.SelectSingleNode("/TDiep/TTChung/MTDiep");
                XmlNode mst_nnt = doc.SelectSingleNode("/TDiep/TTChung/MST");

                Dictionary<string, object> parameters2 = new Dictionary<string, object>();
                parameters2.Add("mtdiep_gui", mtdiep_gui.InnerText);

                string sqlSelectt = $"SELECT * FROM #SCHEMA_NAME#.tonghop_gui_tvan_68 WHERE mtdiep_gui = @mtdiep_gui ";

                DataTable tbl = await _minvoiceDbContext.GetDataTableAsync(sqlSelectt, CommandType.Text, parameters2);

                if (tbl.Rows.Count > 0)
                {
                    for (int i = 0; i < tbl.Rows.Count; i++)
                    {
                        string inv_id = tbl.Rows[i]["type_id"].ToString();
                        string mltdiep_gui = tbl.Rows[i]["mltdiep_gui"].ToString();
                        string mdvi = tbl.Rows[i]["mdvi"]?.ToString();
                        string is_api = tbl.Rows[i]["is_api"]?.ToString();

                        if (mltdiep.InnerText == "102")
                        {
                            XmlNode ket_qua = doc.SelectSingleNode("/TDiep/DLieu/TBao/DLTBao/THop");
                            XmlNode ly_do = doc.SelectSingleNode("/TDiep/DLieu/TBao/DLTBao/DSLDKCNhan");

                            await _minvoiceDbContext.BeginTransactionAsync();

                            string sqlUpdate = $"UPDATE #SCHEMA_NAME#.mau01_68 SET xml_tiepnhan = @xml_rep,"
                                + " ketqua_tiepnhan=@ket_qua, ngay_tbao_tiepnhan=@ngay_tbao,lydo_tiepnhan = @ly_do,"
                                + " mtdiep_tiepnhan_cqt=@mtdiep_cqt, mtdiep_tiepnhan_gui=@mtdiep_gui "
                                + " WHERE mau01_id = @mau01_id";
                            Dictionary<string, object> dic1 = new Dictionary<string, object>();
                            dic1.Add("xml_rep", xml);
                            dic1.Add("mau01_id", Guid.Parse(inv_id));
                            dic1.Add("ket_qua", ket_qua.InnerText);
                            dic1.Add("ngay_tbao", DateTime.Now);
                            dic1.Add("mtdiep_cqt", mtdiep.InnerText);
                            dic1.Add("mtdiep_gui", mtdiep_gui.InnerText);
                            dic1.Add("ly_do", ly_do?.OuterXml);

                            await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, dic1);

                            // update mã thông điệp vào bảng quanly_mathongdiep_cqt
                            string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.quanly_mathongdiep_cqt (id, mtdiep_cqt, xml_cqt, mltdiep, tgian_gui, mdvi) VALUES "
                                          + " (@id ,@mtdiep_cqt, @xml_cqt::xml, @mltdiep, @tgian_gui, @mdvi)";

                            Dictionary<string, object> param_Insert = new Dictionary<string, object>();
                            param_Insert.Add("id", Guid.NewGuid());
                            param_Insert.Add("mtdiep_cqt", mtdiep.InnerText);
                            param_Insert.Add("xml_cqt", xml);
                            param_Insert.Add("mltdiep", mltdiep.InnerText);
                            param_Insert.Add("tgian_gui", DateTime.Now);
                            param_Insert.Add("mdvi", mdvi);

                            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, param_Insert);

                            await _minvoiceDbContext.TransactionCommitAsync();
                            _minvoiceDbContext.CloseTransaction();
                        }
                        else if (mltdiep.InnerText == "103")
                        {
                            XmlNode ket_qua = doc.SelectSingleNode("/TDiep/DLieu/TBao/DLTBao/TTXNCQT");
                            XmlNode ly_do = doc.SelectSingleNode("/TDiep/DLieu/TBao/DLTBao/DSLDKCNhan");

                            await _minvoiceDbContext.BeginTransactionAsync();

                            string sqlUpdate = $"UPDATE #SCHEMA_NAME#.mau01_68 SET xml_rep = @xml_rep,"
                                + " ket_qua=@ket_qua, ngay_tbao=@ngay_tbao,ly_do = @ly_do,"
                                + " mtdiep_cqt=@mtdiep_cqt, mtdiep_gui=@mtdiep_gui "
                                + " WHERE mau01_id = @mau01_id";
                            Dictionary<string, object> dic2 = new Dictionary<string, object>();
                            dic2.Add("xml_rep", xml);
                            dic2.Add("mau01_id", Guid.Parse(inv_id));
                            dic2.Add("ket_qua", ket_qua.InnerText);
                            dic2.Add("ngay_tbao", DateTime.Now);
                            dic2.Add("mtdiep_cqt", mtdiep.InnerText);
                            dic2.Add("mtdiep_gui", mtdiep_gui.InnerText);
                            dic2.Add("ly_do", ly_do?.OuterXml);

                            await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, dic2);

                            // update mã thông điệp vào bảng quanly_mathongdiep_cqt
                            string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.quanly_mathongdiep_cqt (id, mtdiep_cqt, xml_cqt, mltdiep, tgian_gui, mdvi) VALUES "
                                          + " (@id ,@mtdiep_cqt, @xml_cqt::xml, @mltdiep, @tgian_gui, @mdvi)";

                            Dictionary<string, object> param_Insert = new Dictionary<string, object>();
                            param_Insert.Add("id", Guid.NewGuid());
                            param_Insert.Add("mtdiep_cqt", mtdiep.InnerText);
                            param_Insert.Add("xml_cqt", xml);
                            param_Insert.Add("mltdiep", mltdiep.InnerText);
                            param_Insert.Add("tgian_gui", DateTime.Now);
                            param_Insert.Add("mdvi", mdvi);

                            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, param_Insert);

                            await _minvoiceDbContext.TransactionCommitAsync();
                            _minvoiceDbContext.CloseTransaction();
                        }
                        else if (mltdiep.InnerText == "104")
                        {
                            await _minvoiceDbContext.BeginTransactionAsync();

                            string sqlUpdate = $"UPDATE #SCHEMA_NAME#.mau01_68 SET xml_rep = @xml_rep,"
                                + " ket_qua=@ket_qua, ngay_tbao=@ngay_tbao, mtdiep_cqt=@mtdiep_cqt, mtdiep_gui=@mtdiep_gui "
                                + " WHERE mau01_id = @mau01_id";
                            Dictionary<string, object> dic3 = new Dictionary<string, object>();
                            dic3.Add("xml_rep", xml);
                            dic3.Add("mau01_id", Guid.Parse(inv_id));
                            dic3.Add("ket_qua", CommonConstants.TIEP_NHAN_THONG_BAO_UY_NHIEM); // 3 Đã phản hồi kết quả
                            dic3.Add("ngay_tbao", DateTime.Now);
                            dic3.Add("mtdiep_cqt", mtdiep.InnerText);
                            dic3.Add("mtdiep_gui", mtdiep_gui.InnerText);

                            await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, dic3);

                            XmlNodeList dsChapNhan = doc.GetElementsByTagName("TTUNhiem");

                            foreach (XmlNode chapNhan in dsChapNhan)
                            {
                                string sqlUpdateUyNhiem = $"UPDATE #SCHEMA_NAME#.mau01_68_uynhiem SET ket_qua = @ket_qua"
                               + " WHERE mau01_id = @mau01_id and mstunhiem = @mstunhiem and ttcdunhiem = @ttcdunhiem ";
                                Dictionary<string, object> parameters1 = new Dictionary<string, object>();
                                parameters1.Add("ket_qua", chapNhan.SelectSingleNode("DSLDKCNhan") == null ? 1 : 2); // 1 Chấp nhận
                                parameters1.Add("mau01_id", Guid.Parse(inv_id));
                                parameters1.Add("mstunhiem", chapNhan.SelectSingleNode("MST")?.InnerText);
                                parameters1.Add("ttcdunhiem", chapNhan.SelectSingleNode("TTChuc")?.InnerText);

                                await _minvoiceDbContext.TransactionCommandAsync(sqlUpdateUyNhiem, CommandType.Text, parameters1);
                            }

                            // update mã thông điệp vào bảng quanly_mathongdiep_cqt
                            string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.quanly_mathongdiep_cqt (id, mtdiep_cqt, xml_cqt, mltdiep, tgian_gui, mdvi) VALUES "
                                          + " (@id ,@mtdiep_cqt, @xml_cqt::xml, @mltdiep, @tgian_gui, @mdvi)";

                            Dictionary<string, object> param_Insert = new Dictionary<string, object>();
                            param_Insert.Add("id", Guid.NewGuid());
                            param_Insert.Add("mtdiep_cqt", mtdiep.InnerText);
                            param_Insert.Add("xml_cqt", xml);
                            param_Insert.Add("mltdiep", mltdiep.InnerText);
                            param_Insert.Add("tgian_gui", DateTime.Now);
                            param_Insert.Add("mdvi", mdvi);

                            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, param_Insert);

                            await _minvoiceDbContext.TransactionCommitAsync();
                            _minvoiceDbContext.CloseTransaction();
                        }
                        else if (mltdiep.InnerText == "202")
                        {
                            XmlNode maCQT = doc.SelectSingleNode("/TDiep/DLieu/HDon/MCCQT");

                            await _minvoiceDbContext.BeginTransactionAsync();

                            string sqlUpdate = $"UPDATE #SCHEMA_NAME#.hoadon68 SET mccqthue = @mccqthue, tthai = @tthai, mtdiep_gui=@mtdiep_gui, mtdiep_cqt=@mtdiep_cqt \n"
                                            + " WHERE hdon_id = @hdon_id";
                            Dictionary<string, object> param = new Dictionary<string, object>();
                            param.Add("hdon_id", Guid.Parse(inv_id));
                            param.Add("tthai", "Đã cấp mã");
                            param.Add("mccqthue", maCQT.InnerText);
                            param.Add("mtdiep_gui", mtdiep_gui.InnerText);
                            param.Add("mtdiep_cqt", mtdiep.InnerText);

                            await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, param);

                            //XmlNode xmlHdon = doc.SelectSingleNode("/TDiep/DLieu/");

                            string sqlUpdateXML = $"UPDATE #SCHEMA_NAME#.dulieuxml68 SET dlxml_thue = @dlxml_thue"
                            + " WHERE hdon_id = @hdon_id";
                            Dictionary<string, object> param2 = new Dictionary<string, object>();
                            param2.Add("hdon_id", Guid.Parse(inv_id));
                            param2.Add("dlxml_thue", doc.OuterXml);
                            await _minvoiceDbContext.TransactionCommandAsync(sqlUpdateXML, CommandType.Text, param2);

                            // lưu log thông điệp vào bảng tracking_log_message_cqt_68
                            string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.tracking_log_message_cqt_68 (log_message_id, hdon_id, mtdiep_cqt, mtdiep_gui, xml_cqt, loai_tbao, ttchung, tgian_gui) VALUES "
                                  + " (@log_message_id,@hdon_id,@mtdiep_cqt, @mtdiep_gui, @xml_cqt::xml, @loai_tbao, @ttchung, @tgian_gui)";

                            Dictionary<string, object> param_Insert = new Dictionary<string, object>();
                            param_Insert.Add("log_message_id", Guid.NewGuid());
                            param_Insert.Add("hdon_id", Guid.Parse(inv_id));
                            param_Insert.Add("mtdiep_cqt", mtdiep?.InnerText);
                            param_Insert.Add("mtdiep_gui", mtdiep_gui?.InnerText);
                            param_Insert.Add("xml_cqt", xml);
                            param_Insert.Add("loai_tbao", mltdiep.InnerText);
                            param_Insert.Add("ttchung", ttchung.InnerText);
                            param_Insert.Add("tgian_gui", DateTime.Now);

                            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, param_Insert);

                            // update mã thông điệp vào bảng quanly_mathongdiep_cqt
                            sqlInsert = $"INSERT INTO #SCHEMA_NAME#.quanly_mathongdiep_cqt (id, mtdiep_cqt, xml_cqt, mltdiep, tgian_gui, mdvi) VALUES "
                                          + " (@id ,@mtdiep_cqt, @xml_cqt::xml, @mltdiep, @tgian_gui, @mdvi)";

                            param_Insert.Clear();
                            param_Insert.Add("id", Guid.NewGuid());
                            param_Insert.Add("mtdiep_cqt", mtdiep?.InnerText);
                            param_Insert.Add("xml_cqt", xml);
                            param_Insert.Add("mltdiep", mltdiep.InnerText);
                            param_Insert.Add("tgian_gui", DateTime.Now);
                            param_Insert.Add("mdvi", mdvi);

                            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, param_Insert);

                            // lưu kết quả cấp mã hóa đơn
                            string sqlInsert2 = $"INSERT INTO #SCHEMA_NAME#.ketqua_capmahd_68 (ketqua_capmahd_68_id, mltdiepcqt, macqthue, type_id, nguoi_gui, tgian_gui, mtdiep_cqt, mtdtchieu, xml_cqt, note) VALUES "
                                   + " (@ketqua_capmahd_68_id, @mltdiepcqt, @macqthue, @type_id, @nguoi_gui, @tgian_gui, @mtdiep_cqt, @mtdtchieu, @xml_cqt::xml, @note)";

                            Guid ketqua_capmahd_68_id = Guid.NewGuid();

                            param_Insert.Clear();
                            param_Insert.Add("ketqua_capmahd_68_id", ketqua_capmahd_68_id);
                            param_Insert.Add("mltdiepcqt", mltdiep.InnerText);
                            param_Insert.Add("macqthue", maCQT.InnerText);
                            param_Insert.Add("type_id", Guid.Parse(inv_id));
                            param_Insert.Add("nguoi_gui", "Cơ quan Thuế");
                            param_Insert.Add("tgian_gui", DateTime.Now);
                            param_Insert.Add("mtdiep_cqt", mtdiep?.InnerText);
                            param_Insert.Add("mtdtchieu", mtdiep_gui?.InnerText);
                            param_Insert.Add("xml_cqt", xml);
                            param_Insert.Add("note", "capma");

                            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert2, CommandType.Text, param_Insert);

                            await _minvoiceDbContext.TransactionCommitAsync();
                            _minvoiceDbContext.CloseTransaction();

                            //nếu hóa đơn được gửi từ đơn vị khác qua API publish queue gửi hóa đơn id

                            try
                            {
                                if (is_api == "1")
                                {
                                    string[] stringList = siteHddt.Split('.');
                                    string str_mst = stringList[0].ToString();

                                    RabbitClientResponse rabbitClientRes = RabbitClientResponse.GetInstance();
                                    rabbitClientRes.PublishMessageQueue(CommonConstants.QUEUE_MOBIFONE_INVOICE_OUT + str_mst, inv_id);
                                }
                            }
                            catch (Exception ex)
                            {
                                Log.Error(ex.Message);
                            }

                            try
                            {
                                Dictionary<string, object> dicPara = new Dictionary<string, object>();
                                dicPara.Add("branch_code", mdvi);
                                string sql = "SELECT * FROM #SCHEMA_NAME#.wb_setting WHERE (code='AUTO_SEND_EMAIL') AND (branch_code=@branch_code) ORDER BY code";
                                DataTable tblSetting = await _minvoiceDbContext.GetDataTableAsync(sql, CommandType.Text, dicPara);
                                if (tblSetting.Rows.Count > 0)
                                {
                                    string value = tblSetting.Rows[0]["value"].ToString();
                                    if (value == "C")
                                    {
                                        await SendEmail(inv_id, siteHddt);
                                        // update vào bảng hóa đơn đã gửi Email
                                        string sqlUpdateSenMail = "UPDATE #SCHEMA_NAME#.inv_invoiceauth SET isSendmail = true WHERE inv_invoiceauth_id='" + inv_id + "'";
                                        await _minvoiceDbContext.ExecuteNoneQueryAsync(sqlUpdateSenMail);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Log.Error(ex.Message);
                            }
                        }
                        else if (mltdiep.InnerText == "204")
                        {
                            XmlNodeList ds_Lydo = doc.GetElementsByTagName("LDo");

                            if (mltdiep_gui == "400")
                            {
                                // Xử lý bảng tổng hợp dữ liệu
                                XmlNode ketQua = doc.SelectSingleNode("/TDiep/DLieu/TBao/DLTBao/LTBao");

                                await _minvoiceDbContext.BeginTransactionAsync();

                                string sqlUpdate = $"UPDATE #SCHEMA_NAME#.bangtonghopdl_68 SET xml_nhan = @xml_rep, ket_qua = @ket_qua, "
                                    + " ngay_tbao=@ngay_tbao, mtdiepcqt=@mtdiep_cqt, mtdiep_gui=@mtdiep_gui "
                                    + " WHERE bangtonghopdl_68_id = @bangtonghopdl_68_id";
                                Dictionary<string, object> dic5 = new Dictionary<string, object>();
                                dic5.Add("xml_rep", xml);
                                dic5.Add("ket_qua", ketQua.InnerText.ToString() == "2" ? 1 : 2);
                                dic5.Add("bangtonghopdl_68_id", Guid.Parse(inv_id));
                                dic5.Add("ngay_tbao", DateTime.Now);
                                dic5.Add("mtdiep_cqt", mtdiep.InnerText);
                                dic5.Add("mtdiep_gui", mtdiep_gui.InnerText);

                                await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, dic5);

                                // lưu kết quả kiểm tra dữ liệu

                                string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.ketqua_ktdl_68 (ketqua_ktdl_68_id, type_id, mtdiep_cqt, mtdiep_tchieu, xml_cqt, mltdiep_tchieu, tgian_gui, nguoi_gui, note, is_error) VALUES "
                                       + " (@ketqua_ktdl_68_id, @type_id, @mtdiep_cqt, @mtdiep_tchieu, @xml_cqt::xml, @mltdiep_tchieu, @tgian_gui, @nguoi_gui, @note, @is_error)";

                                Guid ketqua_ktdl_68_id = Guid.NewGuid();
                                string is_error = "0";
                                if (ds_Lydo.Count > 0)
                                {
                                    is_error = "1";

                                    // update vào bảng bangtonghopdl_68
                                    //sqlUpdate = $"UPDATE #SCHEMA_NAME#.bangtonghopdl_68 SET error_status = '1' WHERE bangtonghopdl_68_id = '" + inv_id + "'";
                                    //await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, parameters2);
                                }

                                Dictionary<string, object> dic6 = new Dictionary<string, object>();
                                dic6.Add("ketqua_ktdl_68_id", ketqua_ktdl_68_id);
                                dic6.Add("type_id", Guid.Parse(inv_id));
                                dic6.Add("mtdiep_cqt", mtdiep.InnerText);
                                dic6.Add("mtdiep_tchieu", mtdiep_gui.InnerText);
                                dic6.Add("xml_cqt", xml);
                                dic6.Add("mltdiep_tchieu", mltdiep_gui);
                                dic6.Add("tgian_gui", DateTime.Now);
                                dic6.Add("nguoi_gui", "Cơ quan Thuế");
                                dic6.Add("note", "ktdl");
                                dic6.Add("is_error", is_error);

                                await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, dic6);

                                // lưu mã lỗi, hướng dẫn xử lý
                                string maloi = "";
                                string mota = "";
                                string hdxly = "";

                                foreach (XmlNode node in ds_Lydo)
                                {
                                    parameters2.Clear();
                                    parameters2.Add("id", Guid.NewGuid());
                                    parameters2.Add("type_id", Guid.Parse(inv_id));

                                    maloi = node["MLoi"].InnerText;
                                    mota = node["MTLoi"].InnerText;
                                    hdxly = node["HDXLy"]?.InnerText;

                                    parameters2.Clear();
                                    parameters2.Add("id", Guid.NewGuid());
                                    parameters2.Add("type_id", Guid.Parse(inv_id));
                                    parameters2.Add("maloi", maloi);
                                    parameters2.Add("motaloi", mota);
                                    parameters2.Add("huongdanxuly", hdxly);

                                    parameters2.Add("tgian", DateTime.Now);
                                    parameters2.Add("mtdiep_tchieu", mltdiep_gui);
                                    parameters2.Add("reference_id", ketqua_ktdl_68_id);

                                    string sqlInsert1 = $"INSERT INTO #SCHEMA_NAME#.tonghopmaloiphanhoi_68 (id, type_id, maloi, motaloi, tgian, mtdiep_tchieu, huongdanxuly, reference_id) VALUES "
                                                        + " (@id ,@type_id, @maloi, @motaloi, @tgian, @mtdiep_tchieu, @huongdanxuly, @reference_id)";
                                    await _minvoiceDbContext.TransactionCommandAsync(sqlInsert1, CommandType.Text, parameters2);
                                }


                                // update mã thông điệp vào bảng quanly_mathongdiep_cqt
                                //string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.quanly_mathongdiep_cqt (id, mtdiep_cqt, xml_cqt, mltdiep, tgian_gui) VALUES "
                                //              + " (@id ,@mtdiep_cqt, @xml_cqt::xml, @mltdiep, @tgian_gui)";

                                //Dictionary<string, object> param_Insert = new Dictionary<string, object>();
                                //param_Insert.Add("id", Guid.NewGuid());
                                //param_Insert.Add("mtdiep_cqt", mtdiepcqt?.InnerText);
                                //param_Insert.Add("xml_cqt", _res["xml"].ToString());
                                //param_Insert.Add("mltdiep", mltdiep.InnerText);
                                //param_Insert.Add("tgian_gui", DateTime.Now);
                                //await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, param_Insert);

                                //hatm update lại dữ liệu hóa đơn id
                                JObject param_update_hoadon = new JObject();
                                param_update_hoadon.Add("bangtonghopdl_68_id", Guid.Parse(inv_id));
                                string sqlUpdate3 = "#SCHEMA_NAME#.crd_update_hoadon_from_bangdulieu";
                                await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate3, CommandType.StoredProcedure, param_update_hoadon);

                                
                                 //end

                                await _minvoiceDbContext.TransactionCommitAsync();
                                _minvoiceDbContext.CloseTransaction();

                                XmlNodeList ketQuaHoaDon = doc.SelectNodes("/TDiep/DLieu/TBao/DLTBao/LHDKMa/DSHDon/HDon");
                                
                                if (ketQuaHoaDon.Count > 0)
                                {
                                    try
                                    {
                                        await _minvoiceDbContext.BeginTransactionAsync();
                                        for (int k = 0; k < ketQuaHoaDon.Count; k++)
                                        {
                                            XmlNode nodehoadon = ketQuaHoaDon.Item(k);
                                            string khieu = nodehoadon.SelectSingleNode("/KHMSHDon").ToString() + nodehoadon.SelectSingleNode("/KHHDon").ToString();
                                            string shdon = nodehoadon.SelectSingleNode("/SHDon").ToString();

                                            string sqlHoaDon = $@"Select * from #SCHEMA_NAME#.inv_invoiceauth WHERE invoice_series=@khieu and invoice_number=@shdon";
                                            dic6 = new Dictionary<string, object>();
                                            dic6.Add("khieu", khieu);
                                            dic6.Add("shdon", int.Parse(shdon).ToString("D7"));
                                            DataTable hoadon = _minvoiceDbContext.GetDataTable(sqlHoaDon);


                                            // hatm: update trạng thái mã thông điệp vào bảng hoadon68
                                            sqlUpdate = $@"UPDATE #SCHEMA_NAME#.inv_invoiceauth SET \n"
                                               + " error_status = '1', status ='Đã ký'"
                                               + " WHERE inv_invoiceauth_id=@inv_invoiceauth_id";

                                            dic6 = new Dictionary<string, object>();
                                            dic6.Add("inv_invoiceauth_id", hoadon.Rows[0]["inv_invoiceauth_id"]);
                                            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, dic6);


                                            sqlInsert = $"INSERT INTO #SCHEMA_NAME#.ketqua_ktdl_68 (ketqua_ktdl_68_id, type_id, mtdiep_cqt, mtdiep_tchieu, xml_cqt, mltdiep_tchieu, tgian_gui, nguoi_gui, note, is_error) VALUES "
                                               + " (@ketqua_ktdl_68_id, @type_id, @mtdiep_cqt, @mtdiep_tchieu, @xml_cqt::xml, @mltdiep_tchieu, @tgian_gui, @nguoi_gui, @note, @is_error)";

                                            ketqua_ktdl_68_id = Guid.NewGuid();
                                            dic6 = new Dictionary<string, object>();
                                            dic6.Add("ketqua_ktdl_68_id", ketqua_ktdl_68_id);
                                            dic6.Add("type_id", hoadon.Rows[0]["inv_invoiceauth_id"]);
                                            dic6.Add("mtdiep_cqt", mtdiep.InnerText);
                                            dic6.Add("mtdiep_tchieu", mtdiep_gui.InnerText);
                                            dic6.Add("xml_cqt", xml);
                                            dic6.Add("mltdiep_tchieu", mltdiep_gui);
                                            dic6.Add("tgian_gui", DateTime.Now);
                                            dic6.Add("nguoi_gui", "Cơ quan Thuế");
                                            dic6.Add("note", "ktdl");
                                            dic6.Add("is_error", "1");

                                            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, dic6);

                                            // lưu mã lỗi, hướng dẫn xử lý
                                            maloi = "";
                                            mota = "";
                                            hdxly = "";
                                            ds_Lydo = nodehoadon.SelectNodes("/DSLDo/LDo");
                                            foreach (XmlNode node in ds_Lydo)
                                            {
                                                parameters2.Clear();
                                                parameters2.Add("id", Guid.NewGuid());
                                                parameters2.Add("type_id", Guid.Parse(inv_id));

                                                maloi = node["MLoi"].InnerText;
                                                mota = node["MTLoi"].InnerText;
                                                hdxly = node["HDXLy"]?.InnerText;

                                                parameters2.Clear();
                                                parameters2.Add("id", Guid.NewGuid());
                                                parameters2.Add("type_id", Guid.Parse(inv_id));
                                                parameters2.Add("maloi", maloi);
                                                parameters2.Add("motaloi", mota);
                                                parameters2.Add("huongdanxuly", hdxly);

                                                parameters2.Add("tgian", DateTime.Now);
                                                parameters2.Add("mtdiep_tchieu", mltdiep_gui);
                                                parameters2.Add("reference_id", ketqua_ktdl_68_id);

                                                string sqlInsert1 = $"INSERT INTO #SCHEMA_NAME#.tonghopmaloiphanhoi_68 (id, type_id, maloi, motaloi, tgian, mtdiep_tchieu, huongdanxuly, reference_id) VALUES "
                                                                    + " (@id ,@type_id, @maloi, @motaloi, @tgian, @mtdiep_tchieu, @huongdanxuly, @reference_id)";
                                                await _minvoiceDbContext.TransactionCommandAsync(sqlInsert1, CommandType.Text, parameters2);
                                            }
                                        }
                                        await _minvoiceDbContext.TransactionCommitAsync();
                                        _minvoiceDbContext.CloseTransaction();
                                    }
                                    catch (Exception ex)
                                    {
                                        Log.Error(ex.Message);
                                    }
                                }


                            }
                            else if (mltdiep_gui == "300")
                            {
                                // Xử lý hóa đơn

                                await _minvoiceDbContext.BeginTransactionAsync();

                                string sqlSelect_mau04chitiet = $"SELECT mau04_id, khieu, shdon, ladhddt FROM #SCHEMA_NAME#.mau04_68_chitiet \n"
                                                          + " WHERE mau04_id = @mau04_id";
                                Dictionary<string, object> parameters1 = new Dictionary<string, object>();
                                parameters1.Add("mau04_id", Guid.Parse(inv_id));

                                DataTable mau04_chitiet_Table = await _minvoiceDbContext.GetDataTableAsync(sqlSelect_mau04chitiet, CommandType.Text, parameters1);

                                string sqlUpdate = $"UPDATE #SCHEMA_NAME#.mau04_68 SET xml_nhan = @xml_rep,"
                               + " ntbao=@ngay_tbao, mtdiepcqt=@mtdiep_cqt, mtdiep_gui=@mtdiep_gui "
                               + " WHERE mau04_id = @mau04_id";
                                Dictionary<string, object> dic6 = new Dictionary<string, object>();
                                dic6.Add("xml_rep", xml);
                                dic6.Add("mau04_id", Guid.Parse(inv_id));
                                dic6.Add("ngay_tbao", DateTime.Now);
                                dic6.Add("mtdiep_cqt", mtdiep.InnerText);
                                dic6.Add("mtdiep_gui", mtdiep_gui.InnerText);

                                await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, dic6);

                                // lưu kết quả kiểm tra dữ liệu
                                string sqlInsert2 = $"INSERT INTO #SCHEMA_NAME#.ketqua_ktdl_68 (ketqua_ktdl_68_id, type_id, mtdiep_cqt, mtdiep_tchieu, xml_cqt, mltdiep_tchieu, tgian_gui, nguoi_gui, note, is_error) VALUES "
                                       + " (@ketqua_ktdl_68_id, @type_id, @mtdiep_cqt, @mtdiep_tchieu, @xml_cqt::xml, @mltdiep_tchieu, @tgian_gui, @nguoi_gui, @note, @is_error)";

                                Guid ketqua_ktdl_68_id = Guid.NewGuid();
                                string is_error = "0";
                                if (ds_Lydo.Count > 0)
                                {
                                    is_error = "1";
                                }

                                dic6 = new Dictionary<string, object>();
                                dic6.Add("ketqua_ktdl_68_id", ketqua_ktdl_68_id);
                                dic6.Add("type_id", Guid.Parse(inv_id));
                                dic6.Add("mtdiep_cqt", mtdiep.InnerText);
                                dic6.Add("mtdiep_tchieu", mtdiep_gui.InnerText);
                                dic6.Add("xml_cqt", xml);
                                dic6.Add("mltdiep_tchieu", mltdiep_gui);
                                dic6.Add("tgian_gui", DateTime.Now);
                                dic6.Add("nguoi_gui", "Cơ quan Thuế");
                                dic6.Add("note", "ktdl");
                                dic6.Add("is_error", is_error);

                                await _minvoiceDbContext.TransactionCommandAsync(sqlInsert2, CommandType.Text, dic6);

                                XmlNodeList dsHoaDon = doc.GetElementsByTagName("HDon");

                                foreach (XmlNode hoadon in dsHoaDon)
                                {
                                    string xml_hdon = hoadon.OuterXml;
                                    XmlDocument docHd = new XmlDocument();
                                    docHd.PreserveWhitespace = true;
                                    docHd.LoadXml(xml_hdon);

                                    XmlNodeList dsLydoHDon = docHd.GetElementsByTagName("LDo");
                                    XmlNode node1LDo = dsLydoHDon[0];

                                    int tttnhan = 2; // Không chấp nhận

                                    string sqlUpdateChiTiet = $"UPDATE #SCHEMA_NAME#.mau04_68_chitiet SET tttnhan = @tttnhan, ly_do=@ly_do "
                                        + " WHERE mau04_id = @mau04_id and khieu =@khieu and shdon=@shdon ";

                                    parameters2.Clear();
                                    //parameters2.Add("mau04_id", hdon_id);
                                    parameters2.Add("mau04_id", Guid.Parse(inv_id));
                                    parameters2.Add("ly_do", node1LDo?.OuterXml);
                                    parameters2.Add("tttnhan", tttnhan);
                                    parameters2.Add("khieu", hoadon.SelectSingleNode("KHMSHDon")?.InnerText + hoadon.SelectSingleNode("KHHDon")?.InnerText);
                                    parameters2.Add("shdon", int.Parse(hoadon.SelectSingleNode("SHDon")?.InnerText).ToString("D7"));

                                    await _minvoiceDbContext.TransactionCommandAsync(sqlUpdateChiTiet, CommandType.Text, parameters2);

                                    string sqlSelect_Hdondctt = $"SELECT hdon_id, tctbao FROM #SCHEMA_NAME#.mau04_68_chitiet \n"
                                                                + " WHERE mau04_id = @mau04_id and khieu =@khieu and shdon=@shdon";
                                    DataTable hd_dctt_Table = await _minvoiceDbContext.GetDataTableAsync(sqlSelect_Hdondctt, CommandType.Text, parameters2);

                                    if (hd_dctt_Table.Rows.Count > 0)
                                    {
                                        string hd_dctt_id = hd_dctt_Table.Rows[0]["hdon_id"].ToString();
                                        string tctbao = hd_dctt_Table.Rows[0]["tctbao"].ToString();

                                        // QuyenNH: update trạng thái mã thông điệp vào bảng hoadon68
                                        sqlUpdate = $"UPDATE #SCHEMA_NAME#.inv_invoiceauth SET \n"
                                           + " invoice_status = (CASE WHEN @tthai = 'Chấp nhận' then (CASE WHEN @tctbao = '1' THEN 3 WHEN @tctbao = '4' THEN 1 ELSE tthdon END) ELSE tthdon END) ,\n"
                                           + " status=@tthai , mtdiep_gui=@mtdiep_gui , mtdiep_cqt=@mtdiep_cqt \n"
                                           + " WHERE inv_invoiceauth_id = @hdon_id and invoice_series=@khieu and invoice_number=@shdon";

                                        Dictionary<string, object> param = new Dictionary<string, object>();

                                        param.Add("hdon_id", Guid.Parse(hd_dctt_id));
                                        param.Add("tthai", tttnhan == 1 ? "Chấp nhận" : "Không chấp nhận");
                                        param.Add("khieu", hoadon.SelectSingleNode("KHMSHDon")?.InnerText + hoadon.SelectSingleNode("KHHDon")?.InnerText);
                                        param.Add("shdon", int.Parse(hoadon.SelectSingleNode("SHDon")?.InnerText).ToString("D7"));
                                        param.Add("mtdiep_cqt", mtdiep.InnerText);
                                        param.Add("mtdiep_gui", mtdiep_gui.InnerText);
                                        param.Add("tctbao", tctbao);
                                        await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, param);

                                        // lưu log thông điệp vào bảng tracking_log_message_cqt_68
                                        string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.tracking_log_message_cqt_68 (log_message_id, hdon_id, mtdiep_cqt, mtdiep_gui, xml_cqt, loai_tbao, ttchung, tgian_gui) VALUES "
                                              + " (@log_message_id,@hdon_id,@mtdiep_cqt, @mtdiep_gui, @xml_cqt::xml, @loai_tbao, @ttchung, @tgian_gui)";

                                        Dictionary<string, object> param_Insert = new Dictionary<string, object>();
                                        param_Insert.Add("log_message_id", Guid.NewGuid());
                                        param_Insert.Add("hdon_id", Guid.Parse(hd_dctt_id));
                                        param_Insert.Add("mtdiep_cqt", mtdiep?.InnerText);
                                        param_Insert.Add("mtdiep_gui", mtdiep_gui?.InnerText);
                                        param_Insert.Add("xml_cqt", xml);
                                        param_Insert.Add("loai_tbao", mltdiep.InnerText);
                                        param_Insert.Add("ttchung", ttchung.InnerText);
                                        param_Insert.Add("tgian_gui", DateTime.Now);

                                        await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, param_Insert);

                                        // update mã thông điệp vào bảng quanly_mathongdiep_cqt
                                        sqlInsert = $"INSERT INTO #SCHEMA_NAME#.quanly_mathongdiep_cqt (id, mtdiep_cqt, xml_cqt, mltdiep, tgian_gui, mdvi) VALUES "
                                                      + " (@id ,@mtdiep_cqt, @xml_cqt::xml, @mltdiep, @tgian_gui, @mdvi)";

                                        param_Insert.Clear();
                                        param_Insert.Add("id", Guid.NewGuid());
                                        param_Insert.Add("mtdiep_cqt", mtdiep?.InnerText);
                                        param_Insert.Add("xml_cqt", xml);
                                        param_Insert.Add("mltdiep", mltdiep.InnerText);
                                        param_Insert.Add("tgian_gui", DateTime.Now);
                                        param_Insert.Add("mdvi", mdvi);

                                        await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, param_Insert);

                                        // lưu kết quả kiểm tra dữ liệu

                                        sqlInsert2 = $"INSERT INTO #SCHEMA_NAME#.ketqua_ktdl_68 (ketqua_ktdl_68_id, type_id, mtdiep_cqt, mtdiep_tchieu, xml_cqt, mltdiep_tchieu, tgian_gui, nguoi_gui, note, is_error) VALUES "
                                               + " (@ketqua_ktdl_68_id, @type_id, @mtdiep_cqt, @mtdiep_tchieu, @xml_cqt::xml, @mltdiep_tchieu, @tgian_gui, @nguoi_gui, @note, @is_error)";

                                        Guid ketqua_ktdl_68_hdon_id = Guid.NewGuid();

                                        dic6 = new Dictionary<string, object>();
                                        dic6.Add("ketqua_ktdl_68_id", ketqua_ktdl_68_hdon_id);
                                        dic6.Add("type_id", Guid.Parse(hd_dctt_id));
                                        dic6.Add("mtdiep_cqt", mtdiep.InnerText);
                                        dic6.Add("mtdiep_tchieu", mtdiep_gui.InnerText);
                                        dic6.Add("xml_cqt", xml);
                                        dic6.Add("mltdiep_tchieu", mltdiep_gui);
                                        dic6.Add("tgian_gui", DateTime.Now);
                                        dic6.Add("nguoi_gui", "Cơ quan Thuế");
                                        dic6.Add("note", "ktdl");
                                        dic6.Add("is_error", is_error);

                                        await _minvoiceDbContext.TransactionCommandAsync(sqlInsert2, CommandType.Text, dic6);

                                        // lưu mã lỗi, hướng dẫn xử lý
                                        string maloi = "";
                                        string mota = "";
                                        string hdxly = "";

                                        foreach (XmlNode node in dsLydoHDon)
                                        {
                                            maloi = node["MLoi"].InnerText;
                                            mota = node["MTLoi"].InnerText;
                                            hdxly = node["HDXLy"]?.InnerText;

                                            parameters2.Clear();
                                            parameters2.Add("id", Guid.NewGuid());
                                            parameters2.Add("type_id", Guid.Parse(hd_dctt_id));

                                            parameters2.Add("maloi", maloi);
                                            parameters2.Add("motaloi", mota);
                                            parameters2.Add("huongdanxuly", hdxly);

                                            parameters2.Add("tgian", DateTime.Now);
                                            parameters2.Add("mtdiep_tchieu", mltdiep_gui);
                                            parameters2.Add("reference_id", ketqua_ktdl_68_hdon_id);

                                            string sqlInsert1 = $"INSERT INTO #SCHEMA_NAME#.tonghopmaloiphanhoi_68 (id, type_id, maloi, motaloi, tgian, mtdiep_tchieu, huongdanxuly, reference_id) VALUES "
                                                                + " (@id ,@type_id, @maloi, @motaloi, @tgian, @mtdiep_tchieu, @huongdanxuly, @reference_id)";
                                            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert1, CommandType.Text, parameters2);
                                        }

                                        //nếu hóa đơn được gửi từ đơn vị khác qua API publish queue gửi hóa đơn id

                                        try
                                        {
                                            string sqlSelect_hoadon = $"SELECT is_api FROM #SCHEMA_NAME#.tonghop_gui_tvan_68 \n"
                                                       + " WHERE type_id = @hdon_id";
                                            param.Clear();
                                            param.Add("hdon_id", Guid.Parse(hd_dctt_id));
                                            DataTable dt_hoadon = await _minvoiceDbContext.GetDataTableAsync(sqlSelect_hoadon, CommandType.Text, param);
                                            if (dt_hoadon.Rows.Count > 0)
                                            {
                                                if (!string.IsNullOrEmpty(dt_hoadon.Rows[0]["is_api"].ToString()))
                                                {
                                                    if (dt_hoadon.Rows[0]["is_api"].ToString() == "1")
                                                    {
                                                        string[] stringList = siteHddt.Split('.');
                                                        string str_mst = stringList[0].ToString();
                                                        RabbitClientResponse rabbitClientRes = RabbitClientResponse.GetInstance();
                                                        rabbitClientRes.PublishMessageQueue(CommonConstants.QUEUE_MOBIFONE_INVOICE_OUT + str_mst, hd_dctt_id);
                                                    }
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Log.Error(ex.Message);
                                        }

                                    }
                                }

                                await _minvoiceDbContext.TransactionCommitAsync();
                                _minvoiceDbContext.CloseTransaction();

                            }
                            else
                            {
                                // Xử lý hóa đơn

                                //XmlNode mtdiepcqt = doc.SelectSingleNode("/TDiep/TTChung/MTDiep");
                                //XmlNode mtdiepgui = doc.SelectSingleNode("/TDiep/TTChung/MTDTChieu");
                                //XmlNode signature = doc.SelectSingleNode("/TDiep/DLieu/TBao/DSCKS/CQT/Signature");

                                await _minvoiceDbContext.BeginTransactionAsync();

                                string sqlUpdate = $"UPDATE #SCHEMA_NAME#.inv_invoiceauth SET mtdiep_cqt=@mtdiep_cqt, mtdiep_gui=@mtdiep_gui \n"
                                                   + " WHERE inv_invoiceauth_id = @hdon_id";
                                Dictionary<string, object> dic4 = new Dictionary<string, object>();

                                //parameters2.Add("signature", signature.InnerText);
                                dic4.Add("mtdiep_cqt", mtdiep.InnerText);
                                dic4.Add("mtdiep_gui", mtdiep_gui.InnerText);
                                dic4.Add("hdon_id", Guid.Parse(inv_id));

                                await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, dic4);
                                // await _minvoiceDbContext.TransactionCommitAsync();

                                // lưu log thông điệp vào bảng tracking_log_message_cqt_68
                                string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.tracking_log_message_cqt_68 (log_message_id, hdon_id, mtdiep_cqt, mtdiep_gui, xml_cqt, loai_tbao, ttchung, tgian_gui) VALUES "
                                      + " (@log_message_id,@hdon_id,@mtdiep_cqt, @mtdiep_gui, @xml_cqt::xml, @loai_tbao, @ttchung, @tgian_gui)";

                                Dictionary<string, object> param_Insert = new Dictionary<string, object>();
                                param_Insert.Add("log_message_id", Guid.NewGuid());
                                param_Insert.Add("hdon_id", Guid.Parse(inv_id));
                                param_Insert.Add("mtdiep_cqt", mtdiep?.InnerText);
                                param_Insert.Add("mtdiep_gui", mtdiep_gui?.InnerText);
                                param_Insert.Add("xml_cqt", xml);
                                param_Insert.Add("loai_tbao", mltdiep.InnerText);
                                param_Insert.Add("ttchung", ttchung.InnerText);
                                param_Insert.Add("tgian_gui", DateTime.Now);

                                await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, param_Insert);

                                // lưu kết quả kiểm tra dữ liệu

                                string sqlInsert2 = $"INSERT INTO #SCHEMA_NAME#.ketqua_ktdl_68 (ketqua_ktdl_68_id, type_id, mtdiep_cqt, mtdiep_tchieu, xml_cqt, mltdiep_tchieu, tgian_gui, nguoi_gui, note, is_error) VALUES "
                                       + " (@ketqua_ktdl_68_id, @type_id, @mtdiep_cqt, @mtdiep_tchieu, @xml_cqt::xml, @mltdiep_tchieu, @tgian_gui, @nguoi_gui, @note, @is_error)";

                                Guid ketqua_ktdl_68_id = Guid.NewGuid();
                                string is_error = "0";
                                if (ds_Lydo.Count > 0)
                                {
                                    is_error = "1";
                                    // update vào bảng hoadon68
                                    sqlUpdate = $"UPDATE #SCHEMA_NAME#.inv_invoiceauth SET error_status = '1' WHERE inv_invoiceauth_id = '" + inv_id + "'";
                                    await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, new Dictionary<string, object>());
                                }

                                Dictionary<string, object> dic6 = new Dictionary<string, object>();
                                dic6.Add("ketqua_ktdl_68_id", ketqua_ktdl_68_id);
                                dic6.Add("type_id", Guid.Parse(inv_id));
                                dic6.Add("mtdiep_cqt", mtdiep.InnerText);
                                dic6.Add("mtdiep_tchieu", mtdiep_gui.InnerText);
                                dic6.Add("xml_cqt", xml);
                                dic6.Add("mltdiep_tchieu", mltdiep_gui);
                                dic6.Add("tgian_gui", DateTime.Now);
                                dic6.Add("nguoi_gui", "Cơ quan Thuế");
                                dic6.Add("note", "ktdl");
                                dic6.Add("is_error", is_error);

                                await _minvoiceDbContext.TransactionCommandAsync(sqlInsert2, CommandType.Text, dic6);

                                // lưu mã lỗi, hướng dẫn xử lý
                                string maloi = "";
                                string mota = "";
                                string hdxly = "";

                                foreach (XmlNode node in ds_Lydo)
                                {
                                    parameters2.Clear();
                                    parameters2.Add("id", Guid.NewGuid());
                                    parameters2.Add("type_id", Guid.Parse(inv_id));

                                    maloi = node["MLoi"].InnerText;
                                    mota = node["MTLoi"].InnerText;
                                    hdxly = node["HDXLy"]?.InnerText;

                                    parameters2.Clear();
                                    parameters2.Add("id", Guid.NewGuid());
                                    parameters2.Add("type_id", Guid.Parse(inv_id));

                                    parameters2.Add("maloi", maloi);
                                    parameters2.Add("motaloi", mota);
                                    parameters2.Add("huongdanxuly", hdxly);

                                    parameters2.Add("tgian", DateTime.Now);
                                    parameters2.Add("mtdiep_tchieu", mltdiep_gui);
                                    parameters2.Add("reference_id", ketqua_ktdl_68_id);

                                    string sqlInsert1 = $"INSERT INTO #SCHEMA_NAME#.tonghopmaloiphanhoi_68 (id, type_id, maloi, motaloi, tgian, mtdiep_tchieu, huongdanxuly, reference_id) VALUES "
                                                        + " (@id ,@type_id, @maloi, @motaloi, @tgian, @mtdiep_tchieu, @huongdanxuly, @reference_id)";
                                    await _minvoiceDbContext.TransactionCommandAsync(sqlInsert1, CommandType.Text, parameters2);
                                }

                                // update mã thông điệp vào bảng quanly_mathongdiep_cqt
                                sqlInsert = $"INSERT INTO #SCHEMA_NAME#.quanly_mathongdiep_cqt (id, mtdiep_cqt, xml_cqt, mltdiep, tgian_gui, mdvi) VALUES "
                                              + " (@id ,@mtdiep_cqt, @xml_cqt::xml, @mltdiep, @tgian_gui, @mdvi)";

                                param_Insert.Clear();
                                param_Insert.Add("id", Guid.NewGuid());
                                param_Insert.Add("mtdiep_cqt", mtdiep?.InnerText);
                                param_Insert.Add("xml_cqt", xml);
                                param_Insert.Add("mltdiep", mltdiep.InnerText);
                                param_Insert.Add("tgian_gui", DateTime.Now);
                                param_Insert.Add("mdvi", mdvi);

                                await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, param_Insert);

                                await _minvoiceDbContext.TransactionCommitAsync();
                                _minvoiceDbContext.CloseTransaction();

                                //nếu hóa đơn được gửi từ đơn vị khác qua API publish queue gửi hóa đơn id
                                try
                                {
                                    if (is_api == "1")
                                    {
                                        string[] stringList = siteHddt.Split('.');
                                        string str_mst = stringList[0].ToString();
                                        RabbitClientResponse rabbitClientRes = RabbitClientResponse.GetInstance();
                                        rabbitClientRes.PublishMessageQueue(CommonConstants.QUEUE_MOBIFONE_INVOICE_OUT + str_mst, inv_id);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Log.Error(ex.Message);
                                }
                            }
                        }
                        else if (mltdiep.InnerText == "301")
                        {
                            XmlNode ldo = doc.SelectSingleNode("/TDiep/DLieu/TBao/DLTBao/DSLDKTNhan");

                            XmlNodeList dltbao = doc.SelectNodes("/TDiep/DLieu/TBao/DLTBao");

                            await _minvoiceDbContext.BeginTransactionAsync();

                            // get mau04_id trong bảng mau04_68_chietiet

                            string sqlSelect_mau04chitiet = $"SELECT mau04_id, khieu, shdon, ladhddt FROM #SCHEMA_NAME#.mau04_68_chitiet \n"
                                                           + " WHERE mau04_id = @mau04_id";
                            Dictionary<string, object> parameters1 = new Dictionary<string, object>();
                            parameters1.Add("mau04_id", Guid.Parse(inv_id));

                            DataTable mau04_chitiet_Table = await _minvoiceDbContext.GetDataTableAsync(sqlSelect_mau04chitiet, CommandType.Text, parameters1);

                            //string mau04_id = "";
                            //if (mau04_chitiet_Table.Rows.Count == 1)
                            //{
                            //    mau04_id = mau04_chitiet_Table.Rows[0]["mau04_id"].ToString();
                            //}
                            //else if (mau04_chitiet_Table.Rows.Count == 2)
                            //{
                            //    mau04_id = mau04_chitiet_Table.Rows[1]["mau04_id"].ToString();
                            //}

                            string sqlUpdate = $"UPDATE #SCHEMA_NAME#.mau04_68 SET xml_nhan = @xml_rep,"
                                + " ntbao=@ngay_tbao,ly_do = @ly_do, mtdiepcqt=@mtdiep_cqt, mtdiep_gui=@mtdiep_gui "
                                + " WHERE mau04_id = @mau04_id";
                            Dictionary<string, object> dic6 = new Dictionary<string, object>();
                            dic6.Add("xml_rep", xml);
                            //parameters2.Add("mau04_id", hdon_id);
                            dic6.Add("mau04_id", Guid.Parse(inv_id));
                            dic6.Add("ngay_tbao", DateTime.Now);
                            dic6.Add("mtdiep_cqt", mtdiep.InnerText);
                            dic6.Add("mtdiep_gui", mtdiep_gui.InnerText);
                            dic6.Add("ly_do", ldo?.OuterXml);

                            await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, dic6);

                            // check kiểm tra kết quả CQT

                            // nếu kết quả CQT 301 trả về không có hóa đơn nào lỗi thì update tất hóa đơn có trạng thái là chấp nhận

                            foreach (XmlNode dl in dltbao)
                            {
                                XmlNode ttchung_301 = dl["DSHDon"];
                                if (ttchung_301 == null)
                                {
                                    if (mau04_chitiet_Table.Rows.Count > 0)
                                    {
                                        string khhdon = mau04_chitiet_Table.Rows[0]["khieu"].ToString();
                                        string khmshdon = khhdon.Substring(0, 1);
                                        string shdon = mau04_chitiet_Table.Rows[0]["shdon"].ToString();
                                        string ladhddt = mau04_chitiet_Table.Rows[0]["ladhddt"].ToString();

                                        // cập nhật hóa đơn là chấp nhận

                                        string sqlUpdateChiTiet = $"UPDATE #SCHEMA_NAME#.mau04_68_chitiet SET tttnhan = @tttnhan, ly_do=@ly_do "
                                                   + " WHERE mau04_id = @mau04_id and khieu =@khieu and shdon=@shdon and ladhddt=@ladhddt";

                                        parameters2.Clear();
                                        //parameters2.Add("mau04_id", hdon_id);
                                        parameters2.Add("mau04_id", Guid.Parse(inv_id));
                                        parameters2.Add("ly_do", "");
                                        parameters2.Add("tttnhan", 1);
                                        parameters2.Add("khieu", khhdon);
                                        //parameters2.Add("khieu", hoadon.SelectSingleNode("KHHDon")?.InnerText);
                                        parameters2.Add("shdon", shdon);
                                        parameters2.Add("ladhddt", Convert.ToInt32(ladhddt));
                                        //parameters2.Add("tctbao", Convert.ToInt32(hoadon.SelectSingleNode("TCTBao")?.InnerText));

                                        await _minvoiceDbContext.TransactionCommandAsync(sqlUpdateChiTiet, CommandType.Text, parameters2);

                                        string sqlSelect_Hdondctt = $"SELECT hdon_id, tctbao FROM #SCHEMA_NAME#.mau04_68_chitiet \n"
                                                                    + " WHERE mau04_id = @mau04_id and khieu =@khieu and shdon=@shdon and ladhddt=@ladhddt";
                                        DataTable hd_dctt_Table = await _minvoiceDbContext.GetDataTableAsync(sqlSelect_Hdondctt, CommandType.Text, parameters2);

                                        if (hd_dctt_Table.Rows.Count > 0)
                                        {
                                            string hd_dctt_id = hd_dctt_Table.Rows[0]["hdon_id"].ToString();
                                            string tctbao = hd_dctt_Table.Rows[0]["tctbao"].ToString();

                                            // QuyenNH: update trạng thái mã thông điệp vào bảng hoadon68

                                            sqlUpdate = $"UPDATE #SCHEMA_NAME#.inv_invoiceauth SET \n"
                                               + " invoice_status = (CASE WHEN @tthai = 'Chấp nhận' then (CASE WHEN @tctbao = '1' THEN 3 WHEN @tctbao = '4' THEN 1 ELSE tthdon END) ELSE tthdon END) ,\n"
                                               + " status=@tthai , mtdiep_gui=@mtdiep_gui , mtdiep_cqt=@mtdiep_cqt \n"
                                               + " WHERE inv_invoiceauth_id = @hdon_id and invoice_series=@khieu and invoice_number=@shdon";

                                            Dictionary<string, object> param = new Dictionary<string, object>();

                                            param.Add("hdon_id", Guid.Parse(hd_dctt_id));
                                            param.Add("tthai", "Chấp nhận");
                                            param.Add("khieu", khhdon);
                                            param.Add("shdon", int.Parse(shdon).ToString("D7") );
                                            param.Add("mtdiep_cqt", mtdiep.InnerText);
                                            param.Add("mtdiep_gui", mtdiep_gui.InnerText);
                                            param.Add("tctbao", tctbao);
                                            await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, param);

                                            // lưu log thông điệp vào bảng tracking_log_message_cqt_68
                                            string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.tracking_log_message_cqt_68 (log_message_id, hdon_id, mtdiep_cqt, mtdiep_gui, xml_cqt, loai_tbao, ttchung, tgian_gui) VALUES "
                                                  + " (@log_message_id,@hdon_id,@mtdiep_cqt, @mtdiep_gui, @xml_cqt::xml, @loai_tbao, @ttchung, @tgian_gui)";

                                            Dictionary<string, object> param_Insert = new Dictionary<string, object>();
                                            param_Insert.Add("log_message_id", Guid.NewGuid());
                                            param_Insert.Add("hdon_id", Guid.Parse(hd_dctt_id));
                                            param_Insert.Add("mtdiep_cqt", mtdiep?.InnerText);
                                            param_Insert.Add("mtdiep_gui", mtdiep_gui?.InnerText);
                                            param_Insert.Add("xml_cqt", xml);
                                            param_Insert.Add("loai_tbao", mltdiep.InnerText);
                                            param_Insert.Add("ttchung", ttchung.InnerText);
                                            param_Insert.Add("tgian_gui", DateTime.Now);

                                            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, param_Insert);

                                            // update mã thông điệp vào bảng quanly_mathongdiep_cqt
                                            sqlInsert = $"INSERT INTO #SCHEMA_NAME#.quanly_mathongdiep_cqt (id, mtdiep_cqt, xml_cqt, mltdiep, tgian_gui, mdvi) VALUES "
                                                          + " (@id ,@mtdiep_cqt, @xml_cqt::xml, @mltdiep, @tgian_gui, @mdvi)";

                                            param_Insert.Clear();
                                            param_Insert.Add("id", Guid.NewGuid());
                                            param_Insert.Add("mtdiep_cqt", mtdiep?.InnerText);
                                            param_Insert.Add("xml_cqt", xml);
                                            param_Insert.Add("mltdiep", mltdiep.InnerText);
                                            param_Insert.Add("tgian_gui", DateTime.Now);
                                            param_Insert.Add("mdvi", mdvi);

                                            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, param_Insert);

                                            // lưu kết quả xử lý hóa đơn sai sót

                                            string sqlInsert2 = $"INSERT INTO #SCHEMA_NAME#.ketqua_xulyhdsaisot_68 (ketqua_xulyhdsaisot_68_id, mltdiepcqt, type_id, nguoi_gui, tgian_gui, mtdiep_cqt, mtdtchieu, xml_cqt, tttnhan_cqt, note) VALUES "
                                                   + " (@ketqua_xulyhdsaisot_68_id, @mltdiepcqt, @type_id, @nguoi_gui, @tgian_gui, @mtdiep_cqt, @mtdtchieu, @xml_cqt::xml, @tttnhan_cqt, @note)";

                                            Guid ketqua_xulyhdsaisot_68_id = Guid.NewGuid();

                                            param_Insert.Clear();
                                            param_Insert.Add("ketqua_xulyhdsaisot_68_id", ketqua_xulyhdsaisot_68_id);
                                            param_Insert.Add("mltdiepcqt", mltdiep.InnerText);
                                            param_Insert.Add("type_id", Guid.Parse(hd_dctt_id));
                                            param_Insert.Add("nguoi_gui", "Cơ quan Thuế");
                                            param_Insert.Add("tgian_gui", DateTime.Now);
                                            param_Insert.Add("mtdiep_cqt", mtdiep?.InnerText);
                                            param_Insert.Add("mtdtchieu", mtdiep_gui?.InnerText);
                                            param_Insert.Add("xml_cqt", xml);
                                            param_Insert.Add("tttnhan_cqt", "Tiếp nhận");
                                            param_Insert.Add("note", "saisot");

                                            await _minvoiceDbContext.TransactionCommandAsync(sqlInsert2, CommandType.Text, param_Insert);

                                            //nếu hóa đơn được gửi từ đơn vị khác qua API publish queue gửi hóa đơn id

                                            try
                                            {
                                                string sqlSelect_hoadon = $"SELECT is_api FROM #SCHEMA_NAME#.tonghop_gui_tvan_68 \n"
                                                         + " WHERE type_id = @hdon_id";
                                                param.Clear();
                                                param.Add("hdon_id", Guid.Parse(hd_dctt_id));
                                                DataTable dt_hoadon = await _minvoiceDbContext.GetDataTableAsync(sqlSelect_hoadon, CommandType.Text, param);
                                                if (dt_hoadon.Rows.Count > 0)
                                                {
                                                    if (!string.IsNullOrEmpty(dt_hoadon.Rows[0]["is_api"].ToString()))
                                                    {
                                                        if (dt_hoadon.Rows[0]["is_api"].ToString() == "1")
                                                        {
                                                            string[] stringList = siteHddt.Split('.');
                                                            string str_mst = stringList[0].ToString();
                                                            RabbitClientResponse rabbitClientRes = RabbitClientResponse.GetInstance();
                                                            rabbitClientRes.PublishMessageQueue(CommonConstants.QUEUE_MOBIFONE_INVOICE_OUT + str_mst, hd_dctt_id);
                                                        }
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                Log.Error(ex.Message);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    // nếu kết quả CQT 301 trả về có hóa đơn lỗi thì update hóa đơn lỗi là không chấp nhận

                                    XmlNodeList dsHoaDon = doc.GetElementsByTagName("HDon");
                                    if (dsHoaDon.Count > 0)
                                    {
                                        foreach (XmlNode hoadon in dsHoaDon)
                                        {
                                            string xml_hdon = hoadon.OuterXml;
                                            XmlDocument docHd = new XmlDocument();
                                            docHd.PreserveWhitespace = true;
                                            docHd.LoadXml(xml_hdon);

                                            XmlNodeList ds_Lydo = docHd.GetElementsByTagName("LDo");
                                            string tttnccqt = docHd.GetElementsByTagName("TTTNCCQT")[0].InnerText;

                                            //XmlNodeList ds_Lydo = hoadon.SelectNodes("HDon/DSLDKTNhan/LDo");
                                            //XmlNode tttnccqt = hoadon.SelectSingleNode("/DSLDKTNhan/TTTNCCQT");

                                            string status_Tiepnhan = doc.GetElementsByTagName("TTTNCCQT")[0].InnerText;

                                            string sqlUpdateChiTiet = $"UPDATE #SCHEMA_NAME#.mau04_68_chitiet SET tttnhan = @tttnhan, ly_do=@ly_do "
                                                + " WHERE mau04_id = @mau04_id and khieu =@khieu and shdon=@shdon and ladhddt=@ladhddt";

                                            parameters2.Clear();
                                            //parameters2.Add("mau04_id", hdon_id);
                                            parameters2.Add("mau04_id", Guid.Parse(inv_id));
                                            parameters2.Add("ly_do", hoadon.SelectSingleNode("DSLDKTNhan")?.OuterXml);
                                            parameters2.Add("tttnhan", Convert.ToInt32(hoadon.SelectSingleNode("TTTNCCQT")?.InnerText));
                                            parameters2.Add("khieu", hoadon.SelectSingleNode("KHMSHDon")?.InnerText + hoadon.SelectSingleNode("KHHDon")?.InnerText);
                                            //parameters2.Add("khieu", hoadon.SelectSingleNode("KHHDon")?.InnerText);
                                            parameters2.Add("shdon", hoadon.SelectSingleNode("SHDon")?.InnerText);
                                            parameters2.Add("ladhddt", Convert.ToInt32(hoadon.SelectSingleNode("LADHDDT")?.InnerText));
                                            //parameters2.Add("tctbao", Convert.ToInt32(hoadon.SelectSingleNode("TCTBao")?.InnerText));

                                            await _minvoiceDbContext.TransactionCommandAsync(sqlUpdateChiTiet, CommandType.Text, parameters2);

                                            int tttnhan = Convert.ToInt32(hoadon.SelectSingleNode("TTTNCCQT")?.InnerText);
                                            string sqlSelect_Hdondctt = $"SELECT hdon_id, tctbao FROM #SCHEMA_NAME#.mau04_68_chitiet \n"
                                                                        + " WHERE mau04_id = @mau04_id and khieu =@khieu and shdon=@shdon and ladhddt=@ladhddt";
                                            DataTable hd_dctt_Table = await _minvoiceDbContext.GetDataTableAsync(sqlSelect_Hdondctt, CommandType.Text, parameters2);

                                            if (hd_dctt_Table.Rows.Count > 0)
                                            {
                                                string hd_dctt_id = hd_dctt_Table.Rows[0]["hdon_id"].ToString();
                                                string tctbao = hd_dctt_Table.Rows[0]["tctbao"].ToString();

                                                // QuyenNH: update trạng thái mã thông điệp vào bảng hoadon68
                                                sqlUpdate = $"UPDATE #SCHEMA_NAME#.inv_invoiceauth SET \n"
                                                   + " invoice_status = (CASE WHEN @tthai = 'Chấp nhận' then (CASE WHEN @tctbao = '1' THEN 3 WHEN @tctbao = '4' THEN 1 ELSE tthdon END) ELSE tthdon END) ,\n"
                                                   + " status =@tthai , mtdiep_gui=@mtdiep_gui , mtdiep_cqt=@mtdiep_cqt \n"
                                                   + " WHERE inv_invoiceauth_id = @hdon_id and invoice_series=@khieu and invoice_number=@shdon";

                                                Dictionary<string, object> param = new Dictionary<string, object>();

                                                param.Add("hdon_id", Guid.Parse(hd_dctt_id));
                                                param.Add("tthai", tttnhan == 1 ? "Chấp nhận" : "Không chấp nhận");
                                                param.Add("khieu", hoadon.SelectSingleNode("KHMSHDon")?.InnerText + hoadon.SelectSingleNode("KHHDon")?.InnerText);
                                                //param.Add("khieu", hoadon.SelectSingleNode("KHHDon")?.InnerText);
                                                param.Add("shdon", hoadon.SelectSingleNode("SHDon")?.InnerText);
                                                param.Add("mtdiep_cqt", mtdiep.InnerText);
                                                param.Add("mtdiep_gui", mtdiep_gui.InnerText);
                                                param.Add("tctbao", tctbao);
                                                await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, param);

                                                // lưu log thông điệp vào bảng tracking_log_message_cqt_68
                                                string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.tracking_log_message_cqt_68 (log_message_id, hdon_id, mtdiep_cqt, mtdiep_gui, xml_cqt, loai_tbao, ttchung, tgian_gui) VALUES "
                                                      + " (@log_message_id,@hdon_id,@mtdiep_cqt, @mtdiep_gui, @xml_cqt::xml, @loai_tbao, @ttchung, @tgian_gui)";

                                                Dictionary<string, object> param_Insert = new Dictionary<string, object>();
                                                param_Insert.Add("log_message_id", Guid.NewGuid());
                                                param_Insert.Add("hdon_id", Guid.Parse(hd_dctt_id));
                                                param_Insert.Add("mtdiep_cqt", mtdiep?.InnerText);
                                                param_Insert.Add("mtdiep_gui", mtdiep_gui?.InnerText);
                                                param_Insert.Add("xml_cqt", xml);
                                                param_Insert.Add("loai_tbao", mltdiep.InnerText);
                                                param_Insert.Add("ttchung", ttchung.InnerText);
                                                param_Insert.Add("tgian_gui", DateTime.Now);

                                                await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, param_Insert);

                                                // update mã thông điệp vào bảng quanly_mathongdiep_cqt
                                                sqlInsert = $"INSERT INTO #SCHEMA_NAME#.quanly_mathongdiep_cqt (id, mtdiep_cqt, xml_cqt, mltdiep, tgian_gui, mdvi) VALUES "
                                                              + " (@id ,@mtdiep_cqt, @xml_cqt::xml, @mltdiep, @tgian_gui, @mdvi)";

                                                param_Insert.Clear();
                                                param_Insert.Add("id", Guid.NewGuid());
                                                param_Insert.Add("mtdiep_cqt", mtdiep?.InnerText);
                                                param_Insert.Add("xml_cqt", xml);
                                                param_Insert.Add("mltdiep", mltdiep.InnerText);
                                                param_Insert.Add("tgian_gui", DateTime.Now);
                                                param_Insert.Add("mdvi", mdvi);

                                                await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, param_Insert);

                                                // lưu kết quả xử lý hóa đơn sai sót

                                                string sqlInsert2 = $"INSERT INTO #SCHEMA_NAME#.ketqua_xulyhdsaisot_68 (ketqua_xulyhdsaisot_68_id, mltdiepcqt, type_id, nguoi_gui, tgian_gui, mtdiep_cqt, mtdtchieu, xml_cqt, tttnhan_cqt, note) VALUES "
                                                       + " (@ketqua_xulyhdsaisot_68_id, @mltdiepcqt, @type_id, @nguoi_gui, @tgian_gui, @mtdiep_cqt, @mtdtchieu, @xml_cqt::xml, @tttnhan_cqt, @note)";

                                                Guid ketqua_xulyhdsaisot_68_id = Guid.NewGuid();

                                                param_Insert.Clear();
                                                param_Insert.Add("ketqua_xulyhdsaisot_68_id", ketqua_xulyhdsaisot_68_id);
                                                param_Insert.Add("mltdiepcqt", mltdiep.InnerText);
                                                param_Insert.Add("type_id", Guid.Parse(hd_dctt_id));
                                                param_Insert.Add("nguoi_gui", "Cơ quan Thuế");
                                                param_Insert.Add("tgian_gui", DateTime.Now);
                                                param_Insert.Add("mtdiep_cqt", mtdiep?.InnerText);
                                                param_Insert.Add("mtdtchieu", mtdiep_gui?.InnerText);
                                                param_Insert.Add("xml_cqt", xml);
                                                param_Insert.Add("tttnhan_cqt", "Không tiếp nhận");
                                                param_Insert.Add("note", "saisot");

                                                await _minvoiceDbContext.TransactionCommandAsync(sqlInsert2, CommandType.Text, param_Insert);

                                                //nếu hóa đơn được gửi từ đơn vị khác qua API publish queue gửi hóa đơn id

                                                try
                                                {
                                                    string sqlSelect_hoadon = $"SELECT is_api FROM #SCHEMA_NAME#.tonghop_gui_tvan_68 \n"
                                                               + " WHERE type_id = @hdon_id";
                                                    param.Clear();
                                                    param.Add("hdon_id", Guid.Parse(hd_dctt_id));
                                                    DataTable dt_hoadon = await _minvoiceDbContext.GetDataTableAsync(sqlSelect_hoadon, CommandType.Text, param);
                                                    if (dt_hoadon.Rows.Count > 0)
                                                    {
                                                        if (!string.IsNullOrEmpty(dt_hoadon.Rows[0]["is_api"].ToString()))
                                                        {
                                                            if (dt_hoadon.Rows[0]["is_api"].ToString() == "1")
                                                            {
                                                                string[] stringList = siteHddt.Split('.');
                                                                string str_mst = stringList[0].ToString();
                                                                RabbitClientResponse rabbitClientRes = RabbitClientResponse.GetInstance();
                                                                rabbitClientRes.PublishMessageQueue(CommonConstants.QUEUE_MOBIFONE_INVOICE_OUT + str_mst, hd_dctt_id);
                                                            }
                                                        }

                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    Log.Error(ex.Message);
                                                }

                                                // nếu trạng thái không tiếp nhận, ghi log mã lỗi
                                                if (tttnccqt == "2")
                                                {
                                                    string maloi = "";
                                                    string mota = "";

                                                    foreach (XmlNode node in ds_Lydo)
                                                    {
                                                        parameters2.Clear();
                                                        parameters2.Add("id", Guid.NewGuid());
                                                        parameters2.Add("type_id", Guid.Parse(inv_id));

                                                        maloi = node["MLoi"].InnerText;
                                                        mota = node["MTa"].InnerText;
                                                        parameters2.Add("maloi", maloi);
                                                        parameters2.Add("motaloi", mota);
                                                        parameters2.Add("tgian", DateTime.Now);
                                                        parameters2.Add("mtdiep_tchieu", mtdiep_gui?.InnerText);
                                                        parameters2.Add("reference_id", ketqua_xulyhdsaisot_68_id);

                                                        string sqlInsert1 = $"INSERT INTO #SCHEMA_NAME#.tonghopmaloiphanhoi_68 (id, type_id, maloi, motaloi, tgian, mtdiep_tchieu, reference_id) VALUES "
                                                                            + " (@id ,@type_id, @maloi, @motaloi, @tgian, @mtdiep_tchieu, @reference_id)";
                                                        await _minvoiceDbContext.TransactionCommandAsync(sqlInsert1, CommandType.Text, parameters2);
                                                    }
                                                }
                                            }

                                        }
                                    }
                                }
                            }

                            //XmlNodeList dsHoaDon = doc.GetElementsByTagName("HDon");

                            //foreach (XmlNode hoadon in dsHoaDon)
                            //{
                            //    string xml_hdon = hoadon.OuterXml;
                            //    XmlDocument docHd = new XmlDocument();
                            //    docHd.PreserveWhitespace = true;
                            //    docHd.LoadXml(xml_hdon);

                            //    XmlNodeList ds_Lydo = docHd.GetElementsByTagName("LDo");
                            //    string tttnccqt = docHd.GetElementsByTagName("TTTNCCQT")[0].InnerText;

                            //    //XmlNodeList ds_Lydo = hoadon.SelectNodes("HDon/DSLDKTNhan/LDo");
                            //    //XmlNode tttnccqt = hoadon.SelectSingleNode("/DSLDKTNhan/TTTNCCQT");

                            //    string status_Tiepnhan = doc.GetElementsByTagName("TTTNCCQT")[0].InnerText;

                            //    string sqlUpdateChiTiet = $"UPDATE #SCHEMA_NAME#.mau04_68_chitiet SET tttnhan = @tttnhan, ly_do=@ly_do "
                            //        + " WHERE mau04_id = @mau04_id and khieu =@khieu and shdon=@shdon and ladhddt=@ladhddt";

                            //    parameters2.Clear();
                            //    //parameters2.Add("mau04_id", hdon_id);
                            //    parameters2.Add("mau04_id", Guid.Parse(mau04_id));
                            //    parameters2.Add("ly_do", hoadon.SelectSingleNode("DSLDKTNhan")?.OuterXml);
                            //    parameters2.Add("tttnhan", Convert.ToInt32(hoadon.SelectSingleNode("TTTNCCQT")?.InnerText));
                            //    parameters2.Add("khieu", hoadon.SelectSingleNode("KHMSHDon")?.InnerText + hoadon.SelectSingleNode("KHHDon")?.InnerText);
                            //    //parameters2.Add("khieu", hoadon.SelectSingleNode("KHHDon")?.InnerText);
                            //    parameters2.Add("shdon", hoadon.SelectSingleNode("SHDon")?.InnerText);
                            //    parameters2.Add("ladhddt", Convert.ToInt32(hoadon.SelectSingleNode("LADHDDT")?.InnerText));
                            //    //parameters2.Add("tctbao", Convert.ToInt32(hoadon.SelectSingleNode("TCTBao")?.InnerText));

                            //    await _minvoiceDbContext.TransactionCommandAsync(sqlUpdateChiTiet, CommandType.Text, parameters2);

                            //    int tttnhan = Convert.ToInt32(hoadon.SelectSingleNode("TTTNCCQT")?.InnerText);
                            //    string sqlSelect_Hdondctt = $"SELECT hdon_id, tctbao FROM #SCHEMA_NAME#.mau04_68_chitiet \n"
                            //                                + " WHERE mau04_id = @mau04_id and khieu =@khieu and shdon=@shdon and ladhddt=@ladhddt";
                            //    DataTable hd_dctt_Table = await _minvoiceDbContext.GetDataTableAsync(sqlSelect_Hdondctt, CommandType.Text, parameters2);

                            //    if (hd_dctt_Table.Rows.Count > 0)
                            //    {
                            //        string hd_dctt_id = hd_dctt_Table.Rows[0]["hdon_id"].ToString();
                            //        string tctbao = hd_dctt_Table.Rows[0]["tctbao"].ToString();

                            //        // QuyenNH: update trạng thái mã thông điệp vào bảng hoadon68
                            //        sqlUpdate = $"UPDATE #SCHEMA_NAME#.hoadon68 SET \n"
                            //           + " tthdon = (CASE WHEN @tthai = 'Chấp nhận' then (CASE WHEN @tctbao = '1' THEN 3 WHEN @tctbao = '4' THEN 1 ELSE tthdon END) ELSE tthdon END) ,\n"
                            //           + " tthai=@tthai , mtdiep_gui=@mtdiep_gui , mtdiep_cqt=@mtdiep_cqt \n"
                            //           + " WHERE hdon_id = @hdon_id and khieu=@khieu and shdon=@shdon";

                            //        Dictionary<string, object> param = new Dictionary<string, object>();

                            //        param.Add("hdon_id", Guid.Parse(hd_dctt_id));
                            //        param.Add("tthai", tttnhan == 1 ? "Chấp nhận" : "Không chấp nhận");
                            //        param.Add("khieu", hoadon.SelectSingleNode("KHMSHDon")?.InnerText + hoadon.SelectSingleNode("KHHDon")?.InnerText);
                            //        //param.Add("khieu", hoadon.SelectSingleNode("KHHDon")?.InnerText);
                            //        param.Add("shdon", hoadon.SelectSingleNode("SHDon")?.InnerText);
                            //        param.Add("mtdiep_cqt", mtdiep.InnerText);
                            //        param.Add("mtdiep_gui", mtdiep_gui.InnerText);
                            //        param.Add("tctbao", tctbao);
                            //        await _minvoiceDbContext.TransactionCommandAsync(sqlUpdate, CommandType.Text, param);

                            //        // lưu log thông điệp vào bảng tracking_log_message_cqt_68
                            //        string sqlInsert = $"INSERT INTO #SCHEMA_NAME#.tracking_log_message_cqt_68 (log_message_id, hdon_id, mtdiep_cqt, mtdiep_gui, xml_cqt, loai_tbao, ttchung, tgian_gui) VALUES "
                            //              + " (@log_message_id,@hdon_id,@mtdiep_cqt, @mtdiep_gui, @xml_cqt::xml, @loai_tbao, @ttchung, @tgian_gui)";

                            //        Dictionary<string, object> param_Insert = new Dictionary<string, object>();
                            //        param_Insert.Add("log_message_id", Guid.NewGuid());
                            //        param_Insert.Add("hdon_id", Guid.Parse(hd_dctt_id));
                            //        param_Insert.Add("mtdiep_cqt", mtdiep?.InnerText);
                            //        param_Insert.Add("mtdiep_gui", mtdiep_gui?.InnerText);
                            //        param_Insert.Add("xml_cqt", xml);
                            //        param_Insert.Add("loai_tbao", mltdiep.InnerText);
                            //        param_Insert.Add("ttchung", ttchung.InnerText);
                            //        param_Insert.Add("tgian_gui", DateTime.Now);

                            //        await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, param_Insert);

                            //        // update mã thông điệp vào bảng quanly_mathongdiep_cqt
                            //        sqlInsert = $"INSERT INTO #SCHEMA_NAME#.quanly_mathongdiep_cqt (id, mtdiep_cqt, xml_cqt, mltdiep, tgian_gui) VALUES "
                            //                      + " (@id ,@mtdiep_cqt, @xml_cqt::xml, @mltdiep, @tgian_gui)";

                            //        param_Insert.Clear();
                            //        param_Insert.Add("id", Guid.NewGuid());
                            //        param_Insert.Add("mtdiep_cqt", mtdiep?.InnerText);
                            //        param_Insert.Add("xml_cqt", xml);
                            //        param_Insert.Add("mltdiep", mltdiep.InnerText);
                            //        param_Insert.Add("tgian_gui", DateTime.Now);
                            //        await _minvoiceDbContext.TransactionCommandAsync(sqlInsert, CommandType.Text, param_Insert);


                            //        // lưu kết quả xử lý hóa đơn sai sót

                            //        string sqlInsert2 = $"INSERT INTO #SCHEMA_NAME#.ketqua_xulyhdsaisot_68 (ketqua_xulyhdsaisot_68_id, mltdiepcqt, type_id, nguoi_gui, tgian_gui, mtdiep_cqt, mtdtchieu, xml_cqt, tttnhan_cqt, note) VALUES "
                            //               + " (@ketqua_xulyhdsaisot_68_id, @mltdiepcqt, @type_id, @nguoi_gui, @tgian_gui, @mtdiep_cqt, @mtdtchieu, @xml_cqt::xml, @tttnhan_cqt, @note)";

                            //        Guid ketqua_xulyhdsaisot_68_id = Guid.NewGuid();

                            //        param_Insert.Clear();
                            //        param_Insert.Add("ketqua_xulyhdsaisot_68_id", ketqua_xulyhdsaisot_68_id);
                            //        param_Insert.Add("mltdiepcqt", mltdiep.InnerText);
                            //        param_Insert.Add("type_id", Guid.Parse(inv_id));
                            //        param_Insert.Add("nguoi_gui", "Cơ quan Thuế");
                            //        param_Insert.Add("tgian_gui", DateTime.Now);
                            //        param_Insert.Add("mtdiep_cqt", mtdiep?.InnerText);
                            //        param_Insert.Add("mtdtchieu", mtdiep_gui?.InnerText);
                            //        param_Insert.Add("xml_cqt", xml);
                            //        param_Insert.Add("tttnhan_cqt", tttnhan == 1 ? "Tiếp nhận" : "Không tiếp nhận");
                            //        param_Insert.Add("note", "saisot");

                            //        await _minvoiceDbContext.TransactionCommandAsync(sqlInsert2, CommandType.Text, param_Insert);

                            //        // nếu trạng thái không tiếp nhận, ghi log mã lỗi
                            //        if (tttnccqt == "2")
                            //        {
                            //            string maloi = "";
                            //            string mota = "";

                            //            foreach (XmlNode node in ds_Lydo)
                            //            {
                            //                parameters2.Clear();
                            //                parameters2.Add("id", Guid.NewGuid());
                            //                parameters2.Add("type_id", Guid.Parse(inv_id));

                            //                maloi = node["MLoi"].InnerText;
                            //                mota = node["MTa"].InnerText;
                            //                parameters2.Add("maloi", maloi);
                            //                parameters2.Add("motaloi", mota);
                            //                parameters2.Add("tgian", DateTime.Now);
                            //                parameters2.Add("mtdiep_tchieu", mtdiep_gui?.InnerText);
                            //                parameters2.Add("reference_id", ketqua_xulyhdsaisot_68_id);

                            //                string sqlInsert1 = $"INSERT INTO #SCHEMA_NAME#.tonghopmaloiphanhoi_68 (id, type_id, maloi, motaloi, tgian, mtdiep_tchieu, reference_id) VALUES "
                            //                                    + " (@id ,@type_id, @maloi, @motaloi, @tgian, @mtdiep_tchieu, @reference_id)";
                            //                await _minvoiceDbContext.TransactionCommandAsync(sqlInsert1, CommandType.Text, parameters2);
                            //            }
                            //        }
                            //    }

                            //}

                            await _minvoiceDbContext.TransactionCommitAsync();
                            _minvoiceDbContext.CloseTransaction();

                        }

                        await _minvoiceDbContext.BeginTransactionAsync();

                        string insertPhkt = $"INSERT INTO #SCHEMA_NAME#.tonghop_gui_tvan_68_phkt (id, mltdiep_gui, mtdiep_gui, xml_tdiep_gui, mltdtchieu, mtdtchieu, tonghop_gui_tvan_68_id) " +
                        $"  VALUES (@id, @mltdiep_gui, @mtdiep_gui, @xml_tdiep_gui, @mltdtchieu, @mtdtchieu, @tonghop_gui_tvan_68_id)";

                        mtdVendorConfirm = CommonManager.GetMaThongDiep(mst_nnt.InnerText);
                        xmlVendorConfirm = CommonManager.ThongDiepPhanHoi(mltDiepVendorConfirm, mtdVendorConfirm, xml, 0, null);

                        Dictionary<string, object> param6 = new Dictionary<string, object>();
                        param6.Add("id", Guid.NewGuid());
                        param6.Add("mltdiep_gui", mltDiepVendorConfirm);
                        param6.Add("mtdiep_gui", mtdVendorConfirm);
                        param6.Add("xml_tdiep_gui", xmlVendorConfirm);
                        param6.Add("mltdtchieu", mltdiep.InnerText);
                        param6.Add("mtdtchieu", mtdiep.InnerText);
                        param6.Add("tonghop_gui_tvan_68_id", Guid.Parse(tbl.Rows[i]["id"].ToString()));
                        await _minvoiceDbContext.TransactionCommandAsync(insertPhkt, CommandType.Text, param6);

                        await _minvoiceDbContext.TransactionCommitAsync();
                        _minvoiceDbContext.CloseTransaction();
                    }

                }
                else
                {
                    await _minvoiceDbContext.BeginTransactionAsync();

                    string insertPhkt = $"INSERT INTO #SCHEMA_NAME#.tonghop_gui_tvan_68_phkt (id, mltdiep_gui, mtdiep_gui, xml_tdiep_gui, mltdtchieu, mtdtchieu, tonghop_gui_tvan_68_id) " +
                   $"  VALUES (@id, @mltdiep_gui, @mtdiep_gui, @xml_tdiep_gui, @mltdtchieu, @mtdtchieu, @tonghop_gui_tvan_68_id)";

                    mtdVendorConfirm = CommonManager.GetMaThongDiep(mst_nnt.InnerText);

                    JObject obj = new JObject();
                    obj.Add("MLoi", CommonConstants.LOI_KHONG_THAY_THONG_DIEP);
                    obj.Add("MTa", "Không tồn tại thông điệp");
                    JArray dsLoi = new JArray();
                    dsLoi.Add(obj);

                    xmlVendorConfirm = CommonManager.ThongDiepPhanHoi(mltDiepVendorConfirm, mtdVendorConfirm, xml, 1, dsLoi);

                    Dictionary<string, object> param6 = new Dictionary<string, object>();
                    param6.Add("id", Guid.NewGuid());
                    param6.Add("mltdiep_gui", mltDiepVendorConfirm);
                    param6.Add("mtdiep_gui", mtdVendorConfirm);
                    param6.Add("xml_tdiep_gui", xmlVendorConfirm);
                    param6.Add("mltdtchieu", mltdiep.InnerText);
                    param6.Add("mtdtchieu", mtdiep.InnerText);
                    param6.Add("tonghop_gui_tvan_68_id", null);
                    await _minvoiceDbContext.TransactionCommandAsync(insertPhkt, CommandType.Text, param6);

                    await _minvoiceDbContext.TransactionCommitAsync();
                    _minvoiceDbContext.CloseTransaction();
                }

                channel.BasicAck(e.DeliveryTag, false);

                string timestampConfirm = DateTime.Now.ToString("yyyyMMddHHmmssfff");
                string tokenConfirm = FunctionUtil.ComputeSha256Hash(FunctionUtil.ComputeSha256Hash(xmlVendorConfirm) + ":" + mltDiepVendorConfirm + ":" + vendor + ":" + siteHddt + ":" + timestampConfirm + ":" + privateKey);
                string xmlReturn = CommonManager.GenerateXmlPhanHoi(mltDiepVendorConfirm, vendor, siteHddt, timestampConfirm, tokenConfirm, xmlVendorConfirm);

                RabbitClient rabbitClient = RabbitClient.GetInstance();
                rabbitClient.PublishMessageQueue(_queueNameConfirm, xmlReturn);


            }
            catch (Exception ex)
            {
                Log.Error(_queueName + ": " + ex.Message);
                Log.Error(message);

                if (_minvoiceDbContext != null)
                {
                    try
                    {
                        channel.BasicAck(e.DeliveryTag, false);
                    }
                    catch (Exception) { }
                    try
                    {
                        await _minvoiceDbContext.TransactionRollbackAsync();
                    }
                    catch (Exception) { }
                    try
                    {
                        await _minvoiceDbContext.TransactionCommitAsync();
                    }
                    catch (Exception) { }
                    try
                    {
                        _minvoiceDbContext.CloseTransaction();
                    }
                    catch (Exception) { }
                }

            }

        }

        public async Task SendEmail(string inv_invoiceauth_id, string siteHddt)
        {
            try
            {
                EmailSend email = new EmailSend();
                await email.SendInvoiceByEmail(inv_invoiceauth_id, "Gửi hóa đơn", siteHddt);
            }
            catch
            {

            }
        }

        public void CloseTask()
        {
            _channel.Close();
        }


        public bool IsOpen()
        {
            return _channel.IsOpen;
        }


        public string GetTaskName()
        {
            return _queueName;
        }
    }
}
