﻿using InvoiceApiService.Data;
using InvoiceApiService.Util;
using Newtonsoft.Json.Linq;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace InvoiceApiService.RbMQ.Tasks
{
    public class VendorReceiveVanResponse : IRabbitTask
    {
        private string _queueName = CommonConstants.QUEUE_VENDOR_RECEIVE_TCT_RESPONSE;

        private string _queueNameConfirm = CommonConstants.QUEUE_VENDOR_SEND_REQUEST_TO_VAN;

        private IModel _channel;
        //private readonly IMinvoiceDbContext _minvoiceDbContext;

        public VendorReceiveVanResponse()
        {
            //this._minvoiceDbContext = new MInvoiceDbContext();
            InitTask();
        }

        private void InitTask()
        {
            string vendor = CommonConstants.VENDOR;
            RabbitClient client = RabbitClient.GetInstance();
            var conn = client.GetConnection();

            _channel = conn.CreateModel();
            _channel.QueueDeclare(_queueName + "_" + vendor, true, false, false, null);
            _channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);

            var consumer = new EventingBasicConsumer(_channel);
            consumer.ConsumerTag = Guid.NewGuid().ToString();
            consumer.Received += TctResponse;

            _channel.BasicConsume(_queueName + "_" + vendor, false, consumer.ConsumerTag, false, false, null, consumer);
        }

        void TctResponse(object sender, BasicDeliverEventArgs e)
        {
            var message = Encoding.UTF8.GetString(e.Body);
            var channel = ((EventingBasicConsumer)sender).Model;

            channel.BasicAck(e.DeliveryTag, false);

            //IMinvoiceDbContext _minvoiceDbContext = new MInvoiceDbContext();

            try
            {
                XmlDocument docNhan = new XmlDocument();
                docNhan.PreserveWhitespace = true;
                docNhan.LoadXml(message);

                //string cmd = docNhan.GetElementsByTagName("MLTDiep")[0].InnerText;
                string vendor = docNhan.SelectSingleNode("/Body/Vendor").InnerText;
                string siteHddt = docNhan.SelectSingleNode("/Body/Site").InnerText;
                //string timestamp = docNhan.GetElementsByTagName("Timestamp")[0].InnerText;
                //string token = docNhan.GetElementsByTagName("Token")[0].InnerText;
                string xml = docNhan.SelectSingleNode("/Body/DLTDiep").InnerText;

                //_minvoiceDbContext.SetSiteHddt(siteHddt);

                // validate token
                string privateKey = CommonConstants.PRIVATE_KEY;
                //string tokenTmp = FunctionUtil.ComputeSha256Hash(FunctionUtil.ComputeSha256Hash(xml) + ":" + cmd + ":" + vendor + ":" + siteHddt + ":" + timestamp + ":" + privateKey);

                //Message thuế trả về ở đây;
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNode mltdiep = doc.SelectSingleNode("/TDiep/TTChung/MLTDiep");

                if (mltdiep.InnerText == "999" || mltdiep.InnerText == "999V")
                {
                    JObject obj = new JObject();
                    obj.Add("xml", xml);
                    obj.Add("siteHddt", siteHddt);
                    obj.Add("vendor", vendor);
                    obj.Add("privateKey", privateKey);

                    RabbitClient rabbitClientTct = RabbitClient.GetInstance();
                    rabbitClientTct.PublishMessageQueue(CommonConstants.QUEUE_VENDOR_RECEIVE_TCT_RESPONSE_999, obj.ToString());

                }
                else
                {
                    if (mltdiep.InnerText == "302")
                    {

                        JObject obj = new JObject();
                        obj.Add("xml", xml);
                        obj.Add("siteHddt", siteHddt);
                        obj.Add("vendor", vendor);
                        obj.Add("privateKey", privateKey);

                        RabbitClient rabbitClientTct = RabbitClient.GetInstance();
                        rabbitClientTct.PublishMessageQueue(CommonConstants.QUEUE_VENDOR_RECEIVE_TCT_RESPONSE_302, obj.ToString());
                    }
                    else
                    {
                        JObject obj = new JObject();
                        obj.Add("xml", xml);
                        obj.Add("siteHddt", siteHddt);
                        obj.Add("vendor", vendor);
                        obj.Add("privateKey", privateKey);

                        RabbitClient rabbitClientTct = RabbitClient.GetInstance();
                        rabbitClientTct.PublishMessageQueue(CommonConstants.QUEUE_VENDOR_RECEIVE_TCT_RESPONSE_KHAC, obj.ToString());

                    }
                }

            }
            catch (Exception ex)
            {
                Log.Error(_queueName + ": " + ex.Message);
                Log.Error(message);
                //try
                //{
                //    await _minvoiceDbContext.TransactionRollbackAsync();
                //}
                //catch (Exception) { }
                //try
                //{
                //    await _minvoiceDbContext.TransactionCommitAsync();
                //}
                //catch (Exception) { }
                //try
                //{
                //    _minvoiceDbContext.CloseTransaction();
                //}
                //catch (Exception) { }
            }
        }

        //public async Task SendEmail(string inv_invoiceauth_id, string tax_code)
        //{
        //    try
        //    {
        //        EmailSend email = new EmailSend();
        //        await email.SendInvoiceByEmail(inv_invoiceauth_id, tax_code, "Gửi hóa đơn");
        //    }
        //    catch
        //    {

        //    }
        //}

        public void CloseTask()
        {
            _channel.Close();
        }


        public bool IsOpen()
        {
            return _channel.IsOpen;
        }


        public string GetTaskName()
        {
            return _queueName;
        }
    }
}
